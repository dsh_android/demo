package com.example.banmademo.ui.activity;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.alibaba.android.vlayout.DelegateAdapter;
import com.alibaba.android.vlayout.VirtualLayoutManager;
import com.example.banmademo.R;
import com.example.banmademo.adapter.MallBannerAdapter;
import com.example.banmademo.base.BaseActivity;
import com.example.banmademo.bean.HomeImageMessageEntity;
import com.example.banmademo.bean.MallAtmosphereInfo;
import com.example.banmademo.bean.MallAtmosphereResponse;
import com.example.banmademo.bean.MallBannerResponse;
import com.example.banmademo.bean.MallCountdownInfo;
import com.example.banmademo.bean.MallCountdownResponse;
import com.example.banmademo.bean.MallCustomResponse;
import com.example.banmademo.bean.MallIconResponse;
import com.example.banmademo.bean.TodayRecommendResponse;
import com.example.banmademo.factory.HomeLayoutViewFactory;
import com.example.banmademo.home.presenter.HomeImagePresenter;
import com.example.banmademo.home.view.HomeImageView;
import com.example.banmademo.utils.ColorUtil;
import com.example.banmademo.utils.ListUtils;
import com.example.banmademo.widget.XImageView2;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends BaseActivity<HomeImageView, HomeImagePresenter<HomeImageView>> implements HomeImageView {


    public VirtualLayoutManager mVirtualLayoutManager;
    @BindView(R.id.rcl_index)
    RecyclerView rclIndex;
    @BindView(R.id.iv_mall_top_head)
    ImageView ivMallTopHead;
    @BindView(R.id.iv_tab_left_resource)
    ImageView ivTabLeftResource;
    @BindView(R.id.fl_tab_left_resource)
    FrameLayout flTabLeftResource;
    @BindView(R.id.img_index_search)
    ImageView imgIndexSearch;
    @BindView(R.id.text_index_search_word)
    TextView textIndexSearchWord;
    @BindView(R.id.iv_mall_scan)
    ImageView ivMallScan;
    @BindView(R.id.index_search)
    RelativeLayout indexSearch;
    @BindView(R.id.iv_tab_right_resource)
    ImageView ivTabRightResource;
    @BindView(R.id.home_notice)
    ImageView homeNotice;
    @BindView(R.id.rl_home_mall_notice)
    RelativeLayout rlHomeMallNotice;
    @BindView(R.id.index_topLayout)
    RelativeLayout indexTopLayout;
    @BindView(R.id.iv_mall_guide)
    ImageView ivMallGuide;
    @BindView(R.id.rl_root_index_bg)
    RelativeLayout rlRootIndexBg;
    private DelegateAdapter mDelegateAdapter;
    private List<HomeImageMessageEntity.DataBean.DataListBean> homeDataList = new ArrayList<>();

    private MallAtmosphereInfo mMallAtmosphereInfo;
    private int mPromotionInsertPosition;
    private int mSeckillInsertPosition;
    private int mBackToTopPosition;

    @Override
    protected void initView() {
//        mPresent.getHomeImageMessage();
        mPresent.showFirstData();
        mVirtualLayoutManager = new VirtualLayoutManager(this);
        mVirtualLayoutManager.setLayoutViewFactory(HomeLayoutViewFactory.INSTANCE);
        rclIndex.setLayoutManager(mVirtualLayoutManager);
        mDelegateAdapter = new DelegateAdapter(mVirtualLayoutManager, false);
        rclIndex.setAdapter(mDelegateAdapter);
    }

    @Override
    protected void addListener() {

    }

    @Override
    protected void initData() {
//        AppBulletManager.getInstance().requestData(new String[]{"100458326873", "102076920511", "101273278277", "300198587810", "101448442787"});

        resetAndGetMallData();
    }

    public void resetAndGetMallData() {
        this.mPromotionInsertPosition = 0;
        this.mSeckillInsertPosition = 0;
        this.mBackToTopPosition = 0;
//        this.mIsMallPromotionAdded = false;
//        this.mPromotionPageNum = 1;
//        this.mPromotionHasNextPage = false;
//        this.mIsPromotionLoadingMore = false;
//        this.mIsSeckillAdded = false;
//        this.mSeckillPageNum = 1;
//        this.mSeckillHasNextPage = false;
//        this.mIsSeckillLoadingMore = false;
//        MallFloatView mallFloatView = this.mMallFloatView;
//        if (mallFloatView != null) {
//            mallFloatView.processShareShowOrHide(false);
//        }
//        if (this.mPresenter != null) {
//            this.mPresenter.getFirstData();
//            this.mPresenter.getFloatActivityData();
//        }
    }

    private void initMenuRecycler() {
    }


    @Override
    protected HomeImagePresenter<HomeImageView> createPresent() {
        return new HomeImagePresenter<>(this);
    }

    @Override
    protected int attachLayoutRes() {
        return R.layout.activity_main;
    }

    @Override
    public void getHomeImageMessage(HomeImageMessageEntity homeImageMessageEntity) {
    }

    // 第一次获取数据
    @Override
    public void showFirstData(MallAtmosphereResponse mallAtmosphereResponse, MallBannerResponse mallBannerResponse, MallIconResponse mallIconResponse, MallCountdownResponse mallCountdownResponse, MallCountdownResponse mallCountdownResponse2, TodayRecommendResponse todayRecommendResponse, MallCustomResponse mallCustomResponse) {
        Log.e("--kty--", "--请求成功--");
        Log.e("--kty--", "--mallAtmosphereResponse--" + mallAtmosphereResponse.getKey());

        MallCountdownInfo mallCountdownInfo;
        MallCountdownInfo mallCountdownInfo2;
        rclIndex.scrollToPosition(0);
        this.mDelegateAdapter.clear();
        if (mallAtmosphereResponse != null && !ListUtils.isEmpty(mallAtmosphereResponse.getList())) {
            this.mMallAtmosphereInfo = (MallAtmosphereInfo) mallAtmosphereResponse.getList().get(0);
        }
        if (this.mMallAtmosphereInfo == null) {
            this.mMallAtmosphereInfo = new MallAtmosphereInfo();
        }
        setHomeMallBg(this.mMallAtmosphereInfo);
        if (mallBannerResponse != null && !ListUtils.isEmpty(mallBannerResponse.getList())) {
            this.mDelegateAdapter.addAdapter(new MallBannerAdapter(this, this.mMallAtmosphereInfo, mallBannerResponse));
        }



//        if (mallIconResponse != null && !ListUtils.isEmpty(mallIconResponse.getList())) {
//            this.mDelegateAdapter.addAdapter(new MallIconAdapter(this.mContext, this.mMallAtmosphereInfo, mallIconResponse));
//        }
//        if (mallCountdownResponse != null && !ListUtils.isEmpty(mallCountdownResponse.getList()) && (mallCountdownInfo2 = (MallCountdownInfo) mallCountdownResponse.getList().get(0)) != null && mallCountdownInfo2.getExpireSeconds() > 0) {
//            MallCountdownAdapter mallCountdownAdapter = this.mMallActivityCountdownAdapter;
//            if (mallCountdownAdapter != null) {
//                mallCountdownAdapter.closeCountdown();
//            }
//            this.mMallActivityCountdownAdapter = new MallCountdownAdapter(this.mContext, mallCountdownInfo2, "5e35.551e.72e8");
//            this.mDelegateAdapter.addAdapter(this.mMallActivityCountdownAdapter);
//        }
//        if (mallCountdownResponse2 != null && !ListUtils.isEmpty(mallCountdownResponse2.getList()) && (mallCountdownInfo = (MallCountdownInfo) mallCountdownResponse2.getList().get(0)) != null && mallCountdownInfo.getExpireSeconds() > 0) {
//            MallCountdownAdapter mallCountdownAdapter2 = this.mMallCouponCountdownAdapter;
//            if (mallCountdownAdapter2 != null) {
//                mallCountdownAdapter2.closeCountdown();
//            }
//            this.mMallCouponCountdownAdapter = new MallCountdownAdapter(this.mContext, mallCountdownInfo, "5e35.me2d.r8c6");
//            this.mDelegateAdapter.addAdapter(this.mMallCouponCountdownAdapter);
//        }
//        if (LoginManager.getInstance().isMember() && todayRecommendResponse != null && !ListUtils.isEmpty(todayRecommendResponse.getList())) {
//            this.mDelegateAdapter.addAdapter(new MallTodayRecommendAdapter(this.mActivity, (TodayRecommendInfo) todayRecommendResponse.getList().get(0)));
//        }
//        if (mallCustomResponse != null && !ListUtils.isEmpty(mallCustomResponse.getList())) {
//            this.mDelegateAdapter.addAdapter(new MallCustomAdapter(this.mActivity, mallCustomResponse.getList()));
//        }
        this.mPromotionInsertPosition = this.mDelegateAdapter.getItemCount();
        this.mSeckillInsertPosition = this.mDelegateAdapter.getItemCount();
        this.mBackToTopPosition = this.mDelegateAdapter.getItemCount();
        this.mDelegateAdapter.notifyDataSetChanged();


//        if (this.mPresenter != null) {
//            this.mPresenter.getPromotionTitle();
//        }
    }

    public static View initXImageView(Context context) {
        return new XImageView2(context);
    }


    // 设置头部背景颜色
    private void setHomeMallBg(MallAtmosphereInfo mallAtmosphereInfo) {
        if (!TextUtils.isEmpty(mallAtmosphereInfo.getBackColorIndex())) {
            this.rlRootIndexBg.setBackgroundColor(ColorUtil.parseColor(mallAtmosphereInfo.getBackColorIndex()));
        }
    }

}
