package com.example.banmademo;

import androidx.multidex.MultiDexApplication;

/**
 * Create by kty
 * on 2020/5/8
 */
public class MyApplication extends MultiDexApplication {

    @Override
    public void onCreate() {
        super.onCreate();
    }
}
