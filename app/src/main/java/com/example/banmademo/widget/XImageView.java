package com.example.banmademo.widget;

/**
 * Create by kty
 * on 2020/5/8
 */

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;

import com.example.banmademo.utils.StringUtils;

public class XImageView extends YggImageView {
    private static final int HORIZONTAL = 2;
    private static final int MINDIS = 250;
    private static final int NONE = 0;
    private static final int VERTICAL = 1;
    private float mDownX;
    private float mDownY;
    private String selectUrl;
    private String unselectUrl;

    public XImageView(Context context) {
        this(context, (AttributeSet) null);
    }

    public XImageView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public XImageView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    public boolean dispatchTouchEvent(MotionEvent motionEvent) {
        switch (motionEvent.getAction()) {
            case 0:
                this.mDownX = motionEvent.getX();
                this.mDownY = motionEvent.getY();
                break;
            case 2:
                Log.d("CXX", "还在判定中");
                if (((int) Math.sqrt(Math.pow((double) (motionEvent.getX() - this.mDownX), 2.0d) + Math.pow((double) (motionEvent.getY() - this.mDownY), 2.0d))) > MINDIS) {
                    if (Math.abs(motionEvent.getX() - this.mDownX) > Math.abs(motionEvent.getY() - this.mDownY)) {
                        getParent().requestDisallowInterceptTouchEvent(true);
                        return true;
                    }
                    getParent().requestDisallowInterceptTouchEvent(true);
                    return true;
                }
                break;
        }
        return super.dispatchTouchEvent(motionEvent);
    }

    public String getSelectUrl() {
        return !StringUtils.isEmpty(this.selectUrl) ? this.selectUrl : this.unselectUrl;
    }

    public void setSelectUrl(String str) {
        this.selectUrl = str;
    }

    public String getUnselectUrl() {
        return !StringUtils.isEmpty(this.unselectUrl) ? this.unselectUrl : this.selectUrl;
    }

    public void setUnselectUrl(String str) {
        this.unselectUrl = str;
    }
}