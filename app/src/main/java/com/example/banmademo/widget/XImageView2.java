package com.example.banmademo.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.ViewGroup;

/**
 * Create by kty
 * on 2020/5/8
 */
public class XImageView2 extends XImageView {
    public XImageView2(Context context) {
        this(context, (AttributeSet) null);
    }

    public XImageView2(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public XImageView2(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    /* access modifiers changed from: protected */
    public void init() {
        ViewGroup.LayoutParams layoutParams = getLayoutParams();
        if (layoutParams != null) {
            this.mWidth = layoutParams.width > 0 ? layoutParams.width : this.mWidth;
            this.mHeight = layoutParams.height > 0 ? layoutParams.height : this.mHeight;
        }
    }
}
