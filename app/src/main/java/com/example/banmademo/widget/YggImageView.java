package com.example.banmademo.widget;


import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.drawable.Animatable;
import android.net.Uri;
import android.os.Build;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;

import com.example.banmademo.R;
import com.example.banmademo.utils.DeviceUtils;
import com.example.banmademo.utils.ImageConvertUtil;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.backends.pipeline.PipelineDraweeControllerBuilder;
import com.facebook.drawee.controller.ControllerListener;
import com.facebook.drawee.drawable.ScalingUtils;
import com.facebook.drawee.generic.GenericDraweeHierarchy;
import com.facebook.drawee.generic.RoundingParams;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.common.ResizeOptions;
import com.facebook.imagepipeline.image.ImageInfo;
import com.facebook.imagepipeline.image.QualityInfo;
import com.facebook.imagepipeline.request.ImageRequestBuilder;

import java.util.HashMap;

public class YggImageView extends SimpleDraweeView {
    //    private static final int URL_TAG = R.id.url_tag;
    private static final int URL_TAG = 100;
    protected final String TAG;
    private boolean isBackGroundTransparency;
    /* access modifiers changed from: private */
    public boolean isLoaderSuccess;
    private boolean isNeedPlaceholderImage;
    private int mDefaultDuration;
    private int mDefaultFailureResId;
    private int mDefaultHeight;
    private ScalingUtils.ScaleType mDefaultImageScaleType;
    private int mDefaultPlaceholderResId;
    private ScalingUtils.ScaleType mDefaultScaleType;
    private int mDefaultWidth;
    private int mFourItemWidth;
    protected int mHeight;
    private GenericDraweeHierarchy mHierarchy;
    private boolean mIsNeedConvertUrl;
    private int mScreenWidth;
    private boolean mSetPlaceholder;
    private int mSetPlaceholderResId;
    private int mTwoItemWidth;
    protected int mWidth;

    public YggImageView(Context context) {
        this(context, (AttributeSet) null);
    }

    public YggImageView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public YggImageView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        TypedArray obtainStyledAttributes;
        this.TAG = getClass().getSimpleName();
        this.mDefaultScaleType = ScalingUtils.ScaleType.CENTER_INSIDE;
        this.mDefaultImageScaleType = ScalingUtils.ScaleType.CENTER_CROP;
        this.mDefaultFailureResId = R.drawable.ic_launcher_background;
        this.mDefaultPlaceholderResId = R.drawable.ic_launcher_background;
        this.mDefaultDuration = 300;
        this.mSetPlaceholderResId = R.drawable.ic_launcher_background;
        this.mSetPlaceholder = false;
        this.isNeedPlaceholderImage = true;
        this.isBackGroundTransparency = false;
        this.mIsNeedConvertUrl = true;
        if (!(attributeSet == null || (obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, R.styleable.YggImageView)) == null)) {
            this.mIsNeedConvertUrl = obtainStyledAttributes.getBoolean(R.styleable.YggImageView_ygg_need_convert_url, true);
            obtainStyledAttributes.recycle();
        }
        this.mHierarchy = getHierarchy();
        this.mScreenWidth = DeviceUtils.getScreenWidth(context);
        int i2 = this.mScreenWidth;
        this.mDefaultWidth = i2 / 2;
        this.mDefaultHeight = this.mDefaultWidth;
        this.mTwoItemWidth = i2 / 2;
        this.mFourItemWidth = i2 / 4;
        addOnLayoutListener();
    }

    public void setNeedPlaceholderImage(boolean z) {
        this.isNeedPlaceholderImage = z;
    }

    public void setPlaceholderResId(boolean z, int i) {
        this.mSetPlaceholder = z;
        this.mSetPlaceholderResId = i;
    }

    public void addOnLayoutListener() {
        getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            public void onGlobalLayout() {
                int width = YggImageView.this.getWidth();
                int height = YggImageView.this.getHeight();
                if (width > 10 && height > 10) {
                    YggImageView yggImageView = YggImageView.this;
                    yggImageView.mWidth = width;
                    yggImageView.mHeight = height;
                    yggImageView.removeOnGlobalLayoutListener(this);
                }
            }
        });
    }

    /* access modifiers changed from: private */
    public void removeOnGlobalLayoutListener(ViewTreeObserver.OnGlobalLayoutListener onGlobalLayoutListener) {
        if (Build.VERSION.SDK_INT >= 16) {
            getViewTreeObserver().removeOnGlobalLayoutListener(onGlobalLayoutListener);
        } else {
            getViewTreeObserver().removeGlobalOnLayoutListener(onGlobalLayoutListener);
        }
    }

    /* access modifiers changed from: protected */
    public void init() {
        int i = this.mWidth;
        int i2 = this.mTwoItemWidth;
        if (i > i2) {
            this.mDefaultPlaceholderResId = R.drawable.ic_launcher_background;
            this.mDefaultFailureResId = R.drawable.ic_launcher_background;
        } else if (i <= this.mFourItemWidth || i > i2) {
            this.mDefaultPlaceholderResId = R.drawable.ic_launcher_background;
            this.mDefaultFailureResId = R.drawable.ic_launcher_background;
        } else {
            this.mDefaultPlaceholderResId = R.drawable.ic_launcher_background;
            this.mDefaultFailureResId = R.drawable.ic_launcher_background;
        }
        if (!this.isNeedPlaceholderImage) {
            this.mDefaultPlaceholderResId = R.color.transparent;
            this.mDefaultFailureResId = R.color.transparent;
        }
        if (this.mSetPlaceholder) {
            int i3 = this.mSetPlaceholderResId;
            this.mDefaultPlaceholderResId = i3;
            this.mDefaultFailureResId = i3;
        }
        setPlaceholderImage(this.mDefaultPlaceholderResId);
        setFailureImage(this.mDefaultFailureResId);
        this.mHierarchy.setActualImageScaleType(this.mDefaultImageScaleType);
        setFadeDuration(this.mDefaultDuration);
    }

    public void setBackGroundTransparency(boolean z) {
        this.isBackGroundTransparency = z;
    }

    public void setFailureImage(int i) {
        setFailureImage(i, this.mDefaultScaleType);
    }

    public void setFailureImage(int i, ScalingUtils.ScaleType scaleType) {
        this.mHierarchy.setFailureImage(i, scaleType);
    }

    public void setPlaceholderImage(int i) {
        setPlaceholderImage(i, this.mDefaultScaleType);
    }

    public void setPlaceholderImage(int i, ScalingUtils.ScaleType scaleType) {
        this.mHierarchy.setPlaceholderImage(i, scaleType);
    }

    public void setActualImageScaleType(ScalingUtils.ScaleType scaleType) {
        this.mHierarchy.setActualImageScaleType(scaleType);
    }

    public void setImageRadius(float f) {
        this.mHierarchy.setRoundingParams(RoundingParams.fromCornersRadius(f));
    }

    public boolean isRoundAsCircle() {
        RoundingParams roundingParams = this.mHierarchy.getRoundingParams();
        return roundingParams != null && roundingParams.getRoundAsCircle();
    }

    public void setFadeDuration(int i) {
        this.mHierarchy.setFadeDuration(i);
    }

    public void setImageUrl(String str) {
        if (str == null) {
            str = "";
        }
        convertImageUrl((String) null, str, (ControllerListener<ImageInfo>) null);
    }

    public void setImageUrl(String str, String str2) {
        if (str2 == null) {
            str2 = "";
        }
        convertImageUrl(str, str2, (ControllerListener<ImageInfo>) null);
    }

    public void setImageUrl(String str, int i, int i2) {
        setImageUrl((String) null, str, i, i2);
    }

    public void setImageUrl(String str, String str2, int i, int i2) {
        if (str2 == null) {
            str2 = "";
        }
        this.mWidth = i;
        this.mHeight = i2;
        convertImageUrl(str, str2, (ControllerListener<ImageInfo>) null);
    }

    public void setImageUrl(String str, ControllerListener<ImageInfo> controllerListener) {
        convertImageUrl((String) null, str, controllerListener);
    }

    public void setImageUrl(String str, int i, int i2, ControllerListener controllerListener) {
        setImageUrl((String) null, str, i, i2, controllerListener);
    }

    public void setImageUrl(String str, String str2, int i, int i2, ControllerListener controllerListener) {
        if (str2 == null) {
            str2 = "";
        }
        this.mWidth = i;
        this.mHeight = i2;
        convertImageUrl(str, str2, controllerListener);
    }

    public void setImageUrl(String str, String str2, ControllerListener<ImageInfo> controllerListener) {
        convertImageUrl(str, str2, controllerListener);
    }

    /* JADX WARNING: type inference failed for: r1v0, types: [android.widget.ImageView, com.litchi.lib.baseview.YggImageView] */
    public void convertImageUrl(String str, String str2, ControllerListener<ImageInfo> controllerListener) {
        if (this.mIsNeedConvertUrl) {
            int i = this.mWidth;
            if (i > 0) {
                str2 = ImageConvertUtil.convertUrlWithWidth(str2, i);
            } else {
                int i2 = this.mHeight;
                if (i2 > 0) {
                    str2 = ImageConvertUtil.convertUrlWithHeight(str2, i2);
                } else {
                    str2 = ImageConvertUtil.convertUrlWithView(str2, this);
                }
            }
        }
        setImageURI(str != null ? Uri.parse(str) : null, Uri.parse(str2), controllerListener);
    }

    public void setImageResource(int i) {
        setImageURI(new Uri.Builder().scheme("res").path(String.valueOf(i)).build());
    }

    public void setImageURI(Uri uri) {
        setImageURI((Uri) null, uri, (ControllerListener) null);
    }

    private void setImageURI(Uri uri, Uri uri2, ControllerListener controllerListener) {
        if (this.mWidth <= 0 || this.mHeight <= 0) {
            ViewGroup.LayoutParams layoutParams = getLayoutParams();
            if (layoutParams != null) {
                this.mWidth = layoutParams.width > 0 ? layoutParams.width : this.mWidth;
                this.mHeight = layoutParams.height > 0 ? layoutParams.height : this.mHeight;
            }
            if (this.mWidth <= 0) {
                this.mWidth = getMeasuredWidth();
            }
            if (this.mHeight <= 0) {
                this.mHeight = getMeasuredHeight();
            }
            if (this.mWidth <= 0) {
                this.mWidth = this.mDefaultWidth;
            }
            if (this.mHeight <= 0) {
                this.mHeight = this.mDefaultHeight;
            }
            displayImage(uri, uri2, controllerListener);
            return;
        }
        displayImage(uri, uri2, controllerListener);
    }

    private void displayImage(Uri uri, Uri uri2, ControllerListener controllerListener) {
        Object tag = getTag(URL_TAG);
        if (tag == null && !TextUtils.isEmpty(uri2.getPath())) {
            this.isLoaderSuccess = false;
            setTag(URL_TAG, uri2.getPath());
        } else if (tag instanceof String) {
            if (!tag.equals(uri2.getPath()) || !this.isLoaderSuccess) {
                setTag(URL_TAG, uri2.getPath());
            } else if (controllerListener != null) {
                controllerListener.onFinalImageSet("id好像不会用到", new ImageInfo() {
                    public QualityInfo getQualityInfo() {
                        return null;
                    }

                    public int getWidth() {
                        return YggImageView.this.mWidth;
                    }

                    public int getHeight() {
                        return YggImageView.this.mHeight;
                    }
                }, getController().getAnimatable());
                return;
            } else {
                return;
            }
        }
        init();
        PipelineDraweeControllerBuilder oldController = Fresco.newDraweeControllerBuilder().setUri(uri2).setImageRequest(ImageRequestBuilder.newBuilderWithSource(uri2).setResizeOptions(new ResizeOptions(this.mWidth, this.mHeight)).build()).setAutoPlayAnimations(true).setTapToRetryEnabled(false).setControllerListener(new ImageLoaderListener(controllerListener)).setOldController(getController());
        if (uri != null) {
            oldController.setLowResImageRequest(ImageRequestBuilder.newBuilderWithSource(uri).setResizeOptions(new ResizeOptions(this.mWidth, this.mHeight)).build());
        }
        setController(oldController.build());
    }

    private class ImageLoaderListener implements ControllerListener<ImageInfo> {
        private ControllerListener listener;

        public ImageLoaderListener(ControllerListener controllerListener) {
            this.listener = controllerListener;
        }

        public void onSubmit(String str, Object obj) {
            ControllerListener controllerListener = this.listener;
            if (controllerListener != null) {
                controllerListener.onSubmit(str, obj);
            }
        }

        public void onFinalImageSet(String str, ImageInfo imageInfo, Animatable animatable) {
            ControllerListener controllerListener = this.listener;
            if (controllerListener != null) {
                controllerListener.onFinalImageSet(str, imageInfo, animatable);
            }
            boolean unused = YggImageView.this.isLoaderSuccess = true;
        }

        public void onIntermediateImageSet(String str, ImageInfo imageInfo) {
            ControllerListener controllerListener = this.listener;
            if (controllerListener != null) {
                controllerListener.onIntermediateImageSet(str, imageInfo);
            }
        }

        public void onIntermediateImageFailed(String str, Throwable th) {
            ControllerListener controllerListener = this.listener;
            if (controllerListener != null) {
                controllerListener.onIntermediateImageFailed(str, th);
            }
        }

        public void onFailure(String str, Throwable th) {
            ControllerListener controllerListener = this.listener;
            if (controllerListener != null) {
                controllerListener.onFailure(str, th);
            }
        }

        public void onRelease(String str) {
            ControllerListener controllerListener = this.listener;
            if (controllerListener != null) {
                controllerListener.onRelease(str);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        try {
            YggImageView.super.onDraw(canvas);
        } catch (Exception e) {
            Object tag = getTag(URL_TAG);
            if (tag instanceof String) {
                HashMap hashMap = new HashMap();
                hashMap.put("imageUrl", (String) tag);
                hashMap.put("exception", e.getClass().getSimpleName());
                hashMap.put("exception_message", e.getMessage());
            }
        }
    }
}