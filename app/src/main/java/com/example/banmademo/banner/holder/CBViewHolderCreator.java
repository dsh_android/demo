package com.example.banmademo.banner.holder;

public interface CBViewHolderCreator<Holder> {
    Holder createHolder();
}
