package com.example.banmademo.banner.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;

import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.example.banmademo.banner.adapter.CBPageAdapter;
import com.example.banmademo.banner.listener.OnItemClickListener;

public class CBLoopViewPager extends ViewPager {
    private static final float sens = 5.0f;
    private boolean canLoop = true;
    private boolean isCanScroll = true;
    /* access modifiers changed from: private */
    public CBPageAdapter mAdapter;
    ViewPager.OnPageChangeListener mOuterPageChangeListener;
    private float newX = 0.0f;
    private float oldX = 0.0f;
    private OnItemClickListener onItemClickListener;
    private ViewPager.OnPageChangeListener onPageChangeListener = new ViewPager.OnPageChangeListener() {
        private float mPreviousPosition = -1.0f;

        public void onPageSelected(int i) {
            int realPosition = CBLoopViewPager.this.mAdapter.toRealPosition(i);
            float f = (float) realPosition;
            if (this.mPreviousPosition != f) {
                this.mPreviousPosition = f;
                if (CBLoopViewPager.this.mOuterPageChangeListener != null) {
                    CBLoopViewPager.this.mOuterPageChangeListener.onPageSelected(realPosition);
                }
            }
        }

        public void onPageScrolled(int i, float f, int i2) {
            if (CBLoopViewPager.this.mOuterPageChangeListener == null) {
                return;
            }
            if (i != CBLoopViewPager.this.mAdapter.getRealCount() - 1) {
                CBLoopViewPager.this.mOuterPageChangeListener.onPageScrolled(i, f, i2);
            } else if (((double) f) > 0.5d) {
                CBLoopViewPager.this.mOuterPageChangeListener.onPageScrolled(0, 0.0f, 0);
            } else {
                CBLoopViewPager.this.mOuterPageChangeListener.onPageScrolled(i, 0.0f, 0);
            }
        }

        public void onPageScrollStateChanged(int i) {
            if (CBLoopViewPager.this.mOuterPageChangeListener != null) {
                CBLoopViewPager.this.mOuterPageChangeListener.onPageScrollStateChanged(i);
            }
        }
    };

    public void setAdapter(PagerAdapter pagerAdapter, boolean z) {
        this.mAdapter = (CBPageAdapter) pagerAdapter;
        this.mAdapter.setCanLoop(z);
        this.mAdapter.setViewPager(this);
        CBLoopViewPager.super.setAdapter(this.mAdapter);
        setCurrentItem(getFristItem(), false);
    }

    public int getFristItem() {
        if (this.canLoop) {
            return this.mAdapter.getRealCount();
        }
        return 0;
    }

    public int getLastItem() {
        return this.mAdapter.getRealCount() - 1;
    }

    public boolean isCanScroll() {
        return this.isCanScroll;
    }

    public void setCanScroll(boolean z) {
        this.isCanScroll = z;
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (!this.isCanScroll) {
            return false;
        }
        if (this.onItemClickListener != null) {
            switch (motionEvent.getAction()) {
                case 0:
                    this.oldX = motionEvent.getX();
                    break;
                case 1:
                    this.newX = motionEvent.getX();
                    if (Math.abs(this.oldX - this.newX) < sens) {
                        this.onItemClickListener.onItemClick(getRealItem());
                    }
                    this.oldX = 0.0f;
                    this.newX = 0.0f;
                    break;
            }
        }
        return CBLoopViewPager.super.onTouchEvent(motionEvent);
    }

    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        if (this.isCanScroll) {
            return CBLoopViewPager.super.onInterceptTouchEvent(motionEvent);
        }
        return false;
    }

    public CBPageAdapter getAdapter() {
        return this.mAdapter;
    }

    public int getRealItem() {
        CBPageAdapter cBPageAdapter = this.mAdapter;
        if (cBPageAdapter != null) {
            return cBPageAdapter.toRealPosition(CBLoopViewPager.super.getCurrentItem());
        }
        return 0;
    }

    public void setOnPageChangeListener(ViewPager.OnPageChangeListener onPageChangeListener2) {
        this.mOuterPageChangeListener = onPageChangeListener2;
    }

    public CBLoopViewPager(Context context) {
        super(context);
        init();
    }

    public CBLoopViewPager(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        init();
    }

    private void init() {
        CBLoopViewPager.super.setOnPageChangeListener(this.onPageChangeListener);
    }

    public boolean isCanLoop() {
        return this.canLoop;
    }

    public void setCanLoop(boolean z) {
        this.canLoop = z;
        if (!z) {
            setCurrentItem(getRealItem(), false);
        }
        CBPageAdapter cBPageAdapter = this.mAdapter;
        if (cBPageAdapter != null) {
            cBPageAdapter.setCanLoop(z);
            this.mAdapter.notifyDataSetChanged();
        }
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener2) {
        this.onItemClickListener = onItemClickListener2;
    }
}
