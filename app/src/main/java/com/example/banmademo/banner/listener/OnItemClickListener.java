package com.example.banmademo.banner.listener;

public interface OnItemClickListener {
    void onItemClick(int i);
}
