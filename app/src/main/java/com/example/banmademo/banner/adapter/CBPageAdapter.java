package com.example.banmademo.banner.adapter;

import android.view.View;
import android.view.ViewGroup;

import androidx.viewpager.widget.PagerAdapter;

import com.example.banmademo.R;
import com.example.banmademo.banner.holder.CBViewHolderCreator;
import com.example.banmademo.banner.holder.Holder;
import com.example.banmademo.banner.view.CBLoopViewPager;

import java.util.List;

public class CBPageAdapter<T> extends PagerAdapter {
    private final int MULTIPLE_COUNT = 300;
    private boolean canLoop = true;
    protected CBViewHolderCreator holderCreator;
    protected List<T> mDatas;
    private CBLoopViewPager viewPager;

    public boolean isViewFromObject(View view, Object obj) {
        return view == obj;
    }

    public int toRealPosition(int i) {
        int realCount = getRealCount();
        if (realCount == 0) {
            return 0;
        }
        return i % realCount;
    }

    public int getCount() {
        return this.canLoop ? getRealCount() * 300 : getRealCount();
    }

    public int getRealCount() {
        List<T> list = this.mDatas;
        if (list == null) {
            return 0;
        }
        return list.size();
    }

    public Object instantiateItem(ViewGroup viewGroup, int i) {
        View view = getView(toRealPosition(i), (View) null, viewGroup);
        viewGroup.addView(view);
        return view;
    }

    public void destroyItem(ViewGroup viewGroup, int i, Object obj) {
        viewGroup.removeView((View) obj);
    }

    public void finishUpdate(ViewGroup viewGroup) {
        int currentItem = this.viewPager.getCurrentItem();
        if (currentItem == 0) {
            currentItem = this.viewPager.getFristItem();
        } else if (currentItem == getCount() - 1) {
            currentItem = this.viewPager.getLastItem();
        }
        try {
            this.viewPager.setCurrentItem(currentItem, false);
        } catch (IllegalStateException unused) {
        }
    }

    public void setCanLoop(boolean z) {
        this.canLoop = z;
    }

    public void setViewPager(CBLoopViewPager cBLoopViewPager) {
        this.viewPager = cBLoopViewPager;
    }

    public CBPageAdapter(CBViewHolderCreator cBViewHolderCreator, List<T> list) {
        this.holderCreator = cBViewHolderCreator;
        this.mDatas = list;
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        Holder holder;
        View view2;
        if (view == null) {
            holder = (Holder) this.holderCreator.createHolder();
            view2 = holder.createView(viewGroup.getContext());
            view2.setTag(R.id.cb_item_tag, holder);
        } else {
            view2 = view;
//            holder = (Holder) view.getTag(R.id.cb_item_tag);
            holder = (Holder) view.getTag(R.id.cb_item_tag);
        }
        List<T> list = this.mDatas;
        if (list != null && !list.isEmpty()) {
            holder.UpdateUI(viewGroup.getContext(), i, this.mDatas.get(i));
        }
        return view2;
    }
}
