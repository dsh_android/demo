package com.example.banmademo.presenter;

import java.lang.ref.WeakReference;

/**
 * Create by kty
 * on 2020/5/7
 */
public abstract class IBasePresenter<T> {
    /**
     * 持有UI接口的弱引用
     */
    protected WeakReference<T> mViewRef;

    public void attachView(T view) {
        mViewRef = new WeakReference<T>(view);
    }

    /**
     * 解绑
     */
    public void detach() {
        if (mViewRef != null) {
            mViewRef.clear();
            mViewRef = null;
        }
    }

}
