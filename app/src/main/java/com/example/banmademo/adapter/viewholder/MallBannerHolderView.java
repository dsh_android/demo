package com.example.banmademo.adapter.viewholder;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.request.RequestOptions;
import com.example.banmademo.R;
import com.example.banmademo.banner.holder.Holder;
import com.example.banmademo.bean.MallBannerInfo;
import com.example.banmademo.utils.DeviceUtils;
import com.example.banmademo.utils.ImageConvertUtil;
import com.example.banmademo.utils.glide.GlideHelper;
import com.example.banmademo.utils.glide.GlideRoundTransform;

public class MallBannerHolderView implements Holder<MallBannerInfo> {
    private ImageView mImageView;
    private boolean mIsNeedPaddingAndRadius;
    private final RequestOptions mRequestOptions = RequestOptions.placeholderOf(R.mipmap.ic_launcher_round).dontAnimate();

    public MallBannerHolderView(Context context, boolean z) {
        this.mIsNeedPaddingAndRadius = z;
        if (this.mIsNeedPaddingAndRadius) {
            this.mRequestOptions.transform(new GlideRoundTransform(DeviceUtils.dip2Px(context, 4.0f)));
        }
    }

    public View createView(Context context) {
        this.mImageView = new ImageView(context);
        this.mImageView.setLayoutParams(new ViewGroup.LayoutParams(-1, -2));
        if (this.mIsNeedPaddingAndRadius) {
            this.mImageView.setPadding(DeviceUtils.dip2Px(context, 15.0f), 0, DeviceUtils.dip2Px(context, 15.0f), 0);
        }
        this.mImageView.setScaleType(ImageView.ScaleType.FIT_XY);
        return this.mImageView;
    }

    public void UpdateUI(Context context, int i, MallBannerInfo mallBannerInfo) {
        GlideHelper.INSTANCE.loadUrl(context, ImageConvertUtil.convertUrlWithWebp(mallBannerInfo.getImage()), this.mRequestOptions, this.mImageView);
    }
}
