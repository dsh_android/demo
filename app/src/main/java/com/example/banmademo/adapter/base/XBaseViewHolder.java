package com.example.banmademo.adapter.base;

import android.view.View;

import com.example.banmademo.widget.XImageView;

public class XBaseViewHolder extends BaseViewHolder {
    public XBaseViewHolder(View view) {
        super(view);
    }

    public XBaseViewHolder setImageUrl(int i, String str) {
        XImageView view = getView(i);
        if (view != null) {
            view.setImageUrl(str);
        }
        return this;
    }

    public BaseViewHolder setGone(int i, boolean z) {
        getView(i).setVisibility(z ? View.VISIBLE : View.GONE);
        return this;
    }
}
