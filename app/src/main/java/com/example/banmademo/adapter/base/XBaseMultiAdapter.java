package com.example.banmademo.adapter.base;

import android.content.Context;
import android.view.ViewGroup;
import com.chad.library.adapter.base.BaseMultiItemQuickAdapter;
import com.chad.library.adapter.base.entity.MultiItemEntity;
import java.util.List;

public abstract class XBaseMultiAdapter<T extends MultiItemEntity> extends BaseMultiItemQuickAdapter<T, XBaseViewHolder> {
    public XBaseMultiAdapter(Context context) {
        super((List) null);
        this.mContext = context;
        openLoadAnimation();
        setNotDoAnimationCount(10);
//        setLoadMoreView(new XLoadMoreView());
    }

    public void update(List<T> list) {
        setNewData(list);
    }

    public void remove(T t) {
        if (t != null && this.mData.contains(t)) {
            remove(this.mData.indexOf(t));
        }
    }

    public void loadMoreComplete() {
        XBaseMultiAdapter.super.loadMoreComplete();
    }

    public XBaseViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        return XBaseMultiAdapter.super.onCreateViewHolder(viewGroup, i);
    }
}
