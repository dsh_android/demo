package com.example.banmademo.adapter.viewholder;

import android.view.View;

import com.example.banmademo.adapter.base.XBaseViewHolder;

public abstract class IndexBaseViewHolder extends XBaseViewHolder {
    private int indexPosition;
    private int innerPosition;

    public IndexBaseViewHolder(View view) {
        super(view);
    }

    public int getIndexPosition() {
        return this.indexPosition;
    }

    public void setIndexPosition(int i) {
        this.indexPosition = i;
    }

    public int getInnerPosition() {
        return this.innerPosition;
    }

    public void setInnerPosition(int i) {
        this.innerPosition = i;
    }
}
