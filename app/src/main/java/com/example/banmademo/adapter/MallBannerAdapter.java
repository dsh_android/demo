package com.example.banmademo.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.CallSuper;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.UiThread;
import androidx.viewpager.widget.ViewPager;

import com.alibaba.android.vlayout.DelegateAdapter;
import com.alibaba.android.vlayout.LayoutHelper;
import com.alibaba.android.vlayout.layout.SingleLayoutHelper;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.bumptech.glide.request.transition.Transition;
import com.example.banmademo.R;
import com.example.banmademo.adapter.viewholder.IndexBaseViewHolder;
import com.example.banmademo.adapter.viewholder.MallBannerHolderView;
import com.example.banmademo.banner.ConvenientBanner;
import com.example.banmademo.banner.holder.CBViewHolderCreator;
import com.example.banmademo.banner.listener.OnItemClickListener;
import com.example.banmademo.bean.MallAtmosphereInfo;
import com.example.banmademo.bean.MallBannerInfo;
import com.example.banmademo.bean.MallBannerResponse;
import com.example.banmademo.utils.ColorUtil;
import com.example.banmademo.utils.DeviceUtils;
import com.example.banmademo.utils.glide.GlideHelper;
import com.example.banmademo.utils.ImageConvertUtil;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import butterknife.internal.Utils;

/**
 * Create by kty
 * on 2020/5/9
 */
public class MallBannerAdapter extends DelegateAdapter.Adapter<MallBannerAdapter.ViewHolder> {
    public Context mContext;
    public int mIvBgTopHeight;
    private MallAtmosphereInfo mMallAtmosphereInfo;
    private MallBannerResponse mMallBannerResponse;

    public int getItemCount() {
        return 1;
    }

    public int getItemViewType(int i) {
        return 1;
    }

    public class ViewHolder_ViewBinding implements Unbinder {
        private ViewHolder target;

        @UiThread
        public ViewHolder_ViewBinding(ViewHolder viewHolder, View view) {
            this.target = viewHolder;
            viewHolder.indexBanner = (ConvenientBanner) Utils.findRequiredViewAsType(view, R.id.indexBanner, "field 'indexBanner'", ConvenientBanner.class);
            viewHolder.tvCurrentPosition = (TextView) Utils.findRequiredViewAsType(view, R.id.tvCurrentPosition, "field 'tvCurrentPosition'", TextView.class);
            viewHolder.tvSum = (TextView) Utils.findRequiredViewAsType(view, R.id.tvSum, "field 'tvSum'", TextView.class);
            viewHolder.llRootPosition = (LinearLayout) Utils.findRequiredViewAsType(view, R.id.llRootPosition, "field 'llRootPosition'", LinearLayout.class);
            viewHolder.ivBgTop = (ImageView) Utils.findRequiredViewAsType(view, R.id.ivBgTop, "field 'ivBgTop'", ImageView.class);
            viewHolder.ivBgBottom = (ImageView) Utils.findRequiredViewAsType(view, R.id.ivBgBottom, "field 'ivBgBottom'", ImageView.class);
        }

        @CallSuper
        public void unbind() {
            ViewHolder viewHolder = this.target;
            if (viewHolder != null) {
                this.target = null;
                viewHolder.indexBanner = null;
                viewHolder.tvCurrentPosition = null;
                viewHolder.tvSum = null;
                viewHolder.llRootPosition = null;
                viewHolder.ivBgTop = null;
                viewHolder.ivBgBottom = null;
                return;
            }
            throw new IllegalStateException("Bindings already cleared.");
        }
    }

    public MallBannerAdapter(Context context, MallAtmosphereInfo mallAtmosphereInfo, MallBannerResponse mallBannerResponse) {
        this.mContext = context;
        this.mMallAtmosphereInfo = mallAtmosphereInfo;
        this.mMallBannerResponse = mallBannerResponse;
        double screenWidth = (double) DeviceUtils.getScreenWidth(context);
        Double.isNaN(screenWidth);
        this.mIvBgTopHeight = (int) (screenWidth * 0.4444444444444444d);
    }

    public LayoutHelper onCreateLayoutHelper() {
        return new SingleLayoutHelper();
    }

    @NonNull
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        final ViewHolder viewHolder = new ViewHolder(View.inflate(this.mContext, R.layout.layout_banner_item, (ViewGroup) null));
        int screenWidth = DeviceUtils.getScreenWidth(mContext);
        double d = (double) screenWidth;
        Double.isNaN(d);
        viewHolder.itemView.setLayoutParams(new ViewGroup.LayoutParams(screenWidth, (int) (0.5083333333333333d * d)));
        ViewGroup.LayoutParams layoutParams = viewHolder.ivBgTop.getLayoutParams();
        layoutParams.width = screenWidth;
        layoutParams.height = this.mIvBgTopHeight;
        ViewGroup.LayoutParams layoutParams2 = viewHolder.ivBgBottom.getLayoutParams();
        layoutParams2.width = screenWidth;
        Double.isNaN(d);
        layoutParams2.height = (int) (d * 0.06388888888888888d);
        ViewGroup.LayoutParams layoutParams3 = viewHolder.indexBanner.getLayoutParams();
        layoutParams3.width = screenWidth;
        double dip2Px = (double) (screenWidth - DeviceUtils.dip2Px(mContext, 30.0f));
        Double.isNaN(dip2Px);
        layoutParams3.height = (int) (dip2Px * 0.3787878787878788d);
        viewHolder.indexBanner.setOnItemClickListener(new OnItemClickListener() {
            public void onItemClick(int i) {
                MallBannerInfo mallBannerInfo = viewHolder.bannerList.get(i);
                if (mallBannerInfo != null) {
                    // yakaitao
//                    ActionModule type = ActionModule.newInstance().type("2");
//                    IntentManager.getInstance().redirectByScheme(MallBannerAdapter.this.mContext, mallBannerInfo.getUrl());
                }
            }
        });
        viewHolder.indexBanner.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            public void onPageScrollStateChanged(int i) {
            }

            public void onPageScrolled(int i, float f, int i2) {
                if (TextUtils.isEmpty(viewHolder.mallAtmosphereInfo.getBannerBgImgTop())) {
                    int size = i % viewHolder.bannerList.size();
                    int size2 = (i + 1) % viewHolder.bannerList.size();
                    viewHolder.ivBgTop.setBackgroundColor(ColorUtil.excessiveColor(viewHolder.bannerList.get(size).getBgColor(), viewHolder.bannerList.get(size2).getBgColor(), f));
                }
            }

            public void onPageSelected(int i) {
                int i2 = i + 1;
                viewHolder.tvCurrentPosition.setText(String.valueOf(i2));
//                if (MallBannerAdapter.this.mFragment.isResumed() && MallBannerAdapter.this.mFragment.isVisible()) {
//                    ActionModule type = ActionModule.newInstance().type("1");
//                    GTrackMgr.trackEvt(type.spm("5e35.6ab1." + i2).gcmStr(viewHolder.bannerList.get(i).getGcm()));
//                }
            }
        });
        return viewHolder;
    }

    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, int i) {
        viewHolder.mallAtmosphereInfo = this.mMallAtmosphereInfo;
        viewHolder.bannerList = this.mMallBannerResponse.getList();
        GlideHelper.INSTANCE.download(this.mContext, ImageConvertUtil.convertUrlWithView(viewHolder.mallAtmosphereInfo.getBannerBgImgTop(), viewHolder.ivBgTop), new BitmapImageViewTarget(viewHolder.ivBgTop) {
            public void onResourceReady(@NonNull Bitmap bitmap, @Nullable Transition<? super Bitmap> transition) {
                double width = (double) bitmap.getWidth();
                double height = (double) bitmap.getHeight();
                Double.isNaN(width);
                Double.isNaN(height);
                double d = width / height;
                double screenWidth = (double) DeviceUtils.getScreenWidth(mContext);
                double access$200 = (double) MallBannerAdapter.this.mIvBgTopHeight;
                Double.isNaN(screenWidth);
                Double.isNaN(access$200);
                if (d < screenWidth / access$200) {
                    double height2 = (double) bitmap.getHeight();
                    double width2 = (double) bitmap.getWidth();
                    double access$2002 = (double) MallBannerAdapter.this.mIvBgTopHeight;
                    double screenWidth2 = (double) DeviceUtils.getScreenWidth(mContext);
                    Double.isNaN(access$2002);
                    Double.isNaN(screenWidth2);
                    Double.isNaN(width2);
                    Double.isNaN(height2);
                    int i = (int) ((height2 - (width2 * (access$2002 / screenWidth2))) + 0.5d);
                    viewHolder.ivBgTop.setImageBitmap(Bitmap.createBitmap(bitmap, 0, i, bitmap.getWidth(), bitmap.getHeight() - i, (Matrix) null, false));
                    return;
                }
                double width3 = (double) bitmap.getWidth();
                double height3 = (double) bitmap.getHeight();
                double screenWidth3 = (double) DeviceUtils.getScreenWidth(mContext);
                double access$2003 = (double) MallBannerAdapter.this.mIvBgTopHeight;
                Double.isNaN(screenWidth3);
                Double.isNaN(access$2003);
                Double.isNaN(height3);
                Double.isNaN(width3);
                int i2 = (int) ((width3 - (height3 * (screenWidth3 / access$2003))) + 0.5d);
                viewHolder.ivBgTop.setImageBitmap(Bitmap.createBitmap(bitmap, i2, 0, bitmap.getWidth() - i2, bitmap.getHeight(), (Matrix) null, false));
            }
        });
        GlideHelper.INSTANCE.loadUrl(this.mContext, ImageConvertUtil.convertUrlWithView(viewHolder.mallAtmosphereInfo.getBannerBgImgBottom(), viewHolder.ivBgBottom), viewHolder.ivBgBottom);
        if (TextUtils.isEmpty(viewHolder.mallAtmosphereInfo.getBannerBgImgBottom())) {
            viewHolder.ivBgBottom.setBackgroundColor(ColorUtil.parseColor(viewHolder.mallAtmosphereInfo.getBackColorBottom()));
        }
        viewHolder.indexBanner.setPages(new CBViewHolderCreator() {
            public Object createHolder() {
                return new MallBannerHolderView(mContext, true);
            }
        }, viewHolder.bannerList);
        viewHolder.indexBanner.setCanLoop(viewHolder.bannerList.size() > 1);
        int size = viewHolder.bannerList.size();
        if (size > 1) {
            viewHolder.tvCurrentPosition.setText(String.valueOf(1));
            viewHolder.tvSum.setText(String.valueOf(size));
            viewHolder.llRootPosition.setVisibility(View.VISIBLE);
        } else {
            viewHolder.llRootPosition.setVisibility(View.GONE);
        }
        if (!viewHolder.indexBanner.isTurning()) {
            viewHolder.indexBanner.startTurning(4000);
        }
        viewHolder.setInnerPosition(i + 1);
    }

    static class ViewHolder extends IndexBaseViewHolder {
        List<MallBannerInfo> bannerList;
        @BindView(R.id.indexBanner)
        ConvenientBanner indexBanner;
        @BindView(R.id.ivBgBottom)
        ImageView ivBgBottom;
        @BindView(R.id.ivBgTop)
        ImageView ivBgTop;
        @BindView(R.id.llRootPosition)
        LinearLayout llRootPosition;
        MallAtmosphereInfo mallAtmosphereInfo;
        @BindView(R.id.tvCurrentPosition)
        TextView tvCurrentPosition;
        @BindView(R.id.tvSum)
        TextView tvSum;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}
