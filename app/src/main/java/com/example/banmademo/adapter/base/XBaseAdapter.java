package com.example.banmademo.adapter.base;

import android.animation.Animator;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.animation.BaseAnimation;
import java.util.List;

public abstract class XBaseAdapter<T> extends BaseQuickAdapter<T, XBaseViewHolder> {
    /* access modifiers changed from: protected */
    public abstract int getLayoutResId(int i);

    public XBaseAdapter(Context context) {
        super((List) null);
        this.mContext = context;
        this.mLayoutResId = getLayoutResId(0);
        openLoadAnimation();
        setNotDoAnimationCount(10);
//        setLoadMoreView(new XLoadMoreView());
    }

    public void update(List<T> list) {
        setNewData(list);
    }

    public void update(int i) {
        if (i != -1) {
            notifyItemChanged(i + getHeaderLayoutCount());
        }
    }

    public void remove(T t) {
        if (t != null && this.mData.contains(t)) {
            remove(this.mData.indexOf(t));
        }
    }

    public void closeLoadAnimation() {
        openLoadAnimation(new BaseAnimation() {
            public Animator[] getAnimators(View view) {
                return new Animator[0];
            }
        });
    }

    public XBaseViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        this.mLayoutResId = getLayoutResId(i);
        return XBaseAdapter.super.onCreateViewHolder(viewGroup, i);
    }

    public Context getContext() {
        return this.mContext;
    }
}
