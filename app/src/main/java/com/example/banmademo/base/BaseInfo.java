package com.example.banmademo.base;

import com.example.banmademo.bean.MultiItemEntity;

import java.io.Serializable;

public abstract class BaseInfo implements Serializable, MultiItemEntity {
    protected int itemType = 0;

    public int getItemType() {
        return this.itemType;
    }

    public void setItemType(int i) {
        this.itemType = i;
    }
}
