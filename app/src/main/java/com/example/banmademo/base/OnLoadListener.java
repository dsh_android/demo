package com.example.banmademo.base;

import com.example.banmademo.bean.MallAtmosphereResponse;
import com.example.banmademo.bean.MallBannerResponse;
import com.example.banmademo.bean.MallCountdownResponse;
import com.example.banmademo.bean.MallCustomResponse;
import com.example.banmademo.bean.MallIconResponse;
import com.example.banmademo.bean.ResultBaseEntity;
import com.example.banmademo.bean.TodayRecommendResponse;

/**
 * Create by kty
 * on 2020/5/7
 */
public interface OnLoadListener {
    void onComplete(ResultBaseEntity entity);

    void showFirstData(MallAtmosphereResponse mallAtmosphereResponse, MallBannerResponse mallBannerResponse, MallIconResponse mallIconResponse, MallCountdownResponse mallCountdownResponse, MallCountdownResponse mallCountdownResponse2, TodayRecommendResponse todayRecommendResponse, MallCustomResponse mallCustomResponse);
}
