package com.example.banmademo.base;

import android.os.Bundle;

import androidx.annotation.LayoutRes;
import androidx.appcompat.app.AppCompatActivity;

import com.example.banmademo.presenter.IBasePresenter;

import butterknife.ButterKnife;

/**
 * Create by kty
 * on 2020/5/7
 */
public abstract class BaseActivity<V, T extends IBasePresenter<V>> extends AppCompatActivity {

    protected T mPresent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(attachLayoutRes());
        mPresent = createPresent();
        ButterKnife.bind(this);
        initView();
        initData();
        addListener();
    }

    /**
     * 子类实现具体的构建过程
     *
     * @return
     */
    protected abstract T createPresent();

    protected abstract void initView();

    protected abstract void addListener();

    protected abstract void initData();

    /**
     * 绑定布局文件
     *
     * @return 布局文件ID
     */
    @LayoutRes
    protected abstract int attachLayoutRes();

}
