package com.example.banmademo.bean;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class HomeImageMessageEntity extends ResultBaseEntity {


    /**
     * data : {"dataList":[{"code":"101105923976","key":"appHomePageAtmosphere","list":[{"backColorIndex":"#f8f8f8","iconInnerColor":"#ffffff","iconTextColor":"#000000","scrollColor":"#1a1a1a","showIconScroll":true,"hotSelloptColor":"#ffffff","id":39134,"st":"2020-04-26T16:14:18.000+0800","et":"2038-01-19T11:14:07.000+0800","gcm":"2.1.1412f1bc2c78000.129d818dc94cd461.203.0.26_39134."}]},{"code":"100155957578","key":"appHomePageBanner","list":[{"image":"https://img.gegejia.com/newQqbs/c4593689b76d.jpg","width":690,"height":260,"url":"https://wap.myzebravip.com/lego/1588213724058.html?isHide=1&uid=109375","bgColor":"#337b8e","id":40949,"st":"2020-05-07T10:00:00.000+0800","et":"2020-05-09T09:59:59.000+0800","gcm":"2.1.1412f41cbc6e000.3264f2220c1c129d.203.0.26_40949."},{"image":"https://img.gegejia.com/newQqbs/340fdde64f6da.jpg","width":690,"height":260,"url":"https://wap.myzebravip.com/lego/1588213297324.html?isHide=1&uid=109373","bgColor":"#b96f32","id":40739,"st":"2020-05-06T10:00:00.000+0800","et":"2020-05-08T09:59:59.000+0800","gcm":"2.1.1412f41cbc6e000.3264f2220c1c129d.203.0.26_40739."},{"image":"https://img.gegejia.com/newQqbs/696621057da0.jpg","width":690,"height":260,"url":"https://wap.myzebravip.com/lego/1588064484648.html?isHide=1&uid=109244","bgColor":"#a94b4b","id":39885,"st":"2020-05-06T10:00:00.000+0800","et":"2021-05-08T23:59:59.000+0800","gcm":"2.1.1412f41cbc6e000.3264f2220c1c129d.203.0.26_39885."},{"image":"https://img.gegejia.com/newQqbs/340feb0f82c9c.jpg","width":690,"height":260,"url":"https://wap.myzebravip.com/lego/1588213308325.html?isHide=1&uid=109372","bgColor":"#dd742d","id":41022,"st":"2020-05-07T10:00:00.000+0800","et":"2020-05-08T09:59:59.000+0800","gcm":"2.1.1412f41cbc6e000.3264f2220c1c129d.203.0.26_41022."},{"image":"https://img.gegejia.com/newQqbs/2e60a3c0b296a.jpg","width":690,"height":260,"url":"https://wap.myzebravip.com/lego/1588148966577.html?isHide=1&uid=109335","bgColor":"#e6baad","id":33059,"st":"2020-05-07T10:00:00.000+0800","et":"2020-05-08T09:59:59.000+0800","gcm":"2.1.1412f41cbc6e000.3264f2220c1c129d.203.0.26_33059."},{"image":"https://img.gegejia.com/newQqbs/11f4b154cd637.jpg","width":690,"height":260,"url":"https://wap.myzebravip.com/lego/1587019346126.html?isHide=1&uid=108701","bgColor":"#132247","id":40420,"st":"2020-05-07T10:00:00.000+0800","et":"2020-05-08T09:59:59.000+0800","gcm":"2.1.1412f41cbc6e000.3264f2220c1c129d.203.0.26_40420."},{"image":"https://img.gegejia.com/newQqbs/17a3e25ecd81f.jpg","width":690,"height":260,"url":"https://wap.myzebravip.com/lego/1587368500310.html?isHide=1&uid=108849","bgColor":"#174d62","id":40423,"st":"2020-05-07T10:00:00.000+0800","et":"2020-05-08T09:59:59.000+0800","gcm":"2.1.1412f41cbc6e000.3264f2220c1c129d.203.0.26_40423."},{"image":"https://img.gegejia.com/newQqbs/c45805028036.jpg","width":690,"height":260,"url":"https://wap.myzebravip.com/lego/1587021198698.html?isHide=1&uid=108705","bgColor":"#7c3824","id":40424,"st":"2020-05-07T10:00:00.000+0800","et":"2020-05-08T09:59:59.000+0800","gcm":"2.1.1412f41cbc6e000.3264f2220c1c129d.203.0.26_40424."}]},{"code":"100936418014","key":"appHomePageIcon","list":[{"title":"发现","icon":"https://img.gegejia.com/newQqbs/340f0cf6d3d71.png","width":207,"height":207,"url":"https://wap.myzebravip.com/discovery/index/","id":27678,"st":"2020-04-05T00:00:00.000+0800","et":"2038-01-19T11:14:07.000+0800","gcm":"2.1.1412f2052071000.e9f972377c23d863.203.0.26_27678."},{"title":"新人红包","icon":"https://img.gegejia.com/newQqbs/17a31b05b3a8e.png","width":207,"height":207,"url":"https://wap.myzebravip.com/lego/1570498303615.html?isHide=1&uid=100743","id":27832,"st":"2020-04-11T00:00:00.000+0800","et":"2038-01-19T11:14:07.000+0800","gcm":"2.1.1412f2052071000.e9f972377c23d863.203.0.26_27832."},{"title":"斑马出品","icon":"https://img.gegejia.com/newQqbs/230179e0c7149.png","width":207,"height":207,"url":"https://wap.myzebravip.com/lego/1573810926223.html?isHide=1&uid=103081","id":27681,"st":"2020-04-05T00:00:00.000+0800","et":"2038-01-19T11:14:07.000+0800","gcm":"2.1.1412f2052071000.e9f972377c23d863.203.0.26_27681."},{"title":"积分商品","icon":"https://img.gegejia.com/newQqbs/1d5248d9c93c7.gif","width":207,"height":207,"url":"https://wap.myzebravip.com/buyer_lego/lego/1582538393644.html?isHide=1&uid=106627","h5NotJump":false,"id":27682,"st":"2020-04-05T00:00:00.000+0800","et":"2038-01-19T11:14:07.000+0800","gcm":"2.1.1412f2052071000.e9f972377c23d863.203.0.26_27682."},{"title":"中国田","icon":"https://img.gegejia.com/newQqbs/69584bbfa5a0.png","width":207,"height":207,"url":"https://wap.myzebravip.com/lego/1575509094636.html?isHide=1&uid=104150","id":27684,"st":"2020-04-05T00:00:00.000+0800","et":"2038-01-19T11:14:07.000+0800","gcm":"2.1.1412f2052071000.e9f972377c23d863.203.0.26_27684."},{"title":"斑马优选","icon":"https://img.gegejia.com/newQqbs/17a317d3a5bc8.gif","width":207,"height":207,"url":"https://wap.myzebravip.com/lego/1588045165385.html?isHide=1&uid=109219","id":27685,"st":"2020-04-05T00:00:00.000+0800","et":"2038-01-19T11:14:07.000+0800","gcm":"2.1.1412f2052071000.e9f972377c23d863.203.0.26_27685."},{"title":"斑马果园","icon":"https://img.gegejia.com/newQqbs/17a317d42ccc9.png","width":207,"height":207,"url":"https://wap.myzebravip.com/orchard/","id":27686,"st":"2020-04-05T00:00:00.000+0800","et":"2038-01-19T11:14:07.000+0800","gcm":"2.1.1412f2052071000.e9f972377c23d863.203.0.26_27686."},{"title":"斑马CBT","icon":"https://img.gegejia.com/newQqbs/28b0e85083db7.png","width":207,"height":207,"url":"https://x.myzebravip.com/lego/1585303324372.html?isHide=1&uid=107841","id":27458,"st":"2020-04-05T00:00:00.000+0800","et":"2038-01-19T11:14:07.000+0800","gcm":"2.1.1412f2052071000.e9f972377c23d863.203.0.26_27458."},{"title":"热销榜单","icon":"https://img.gegejia.com/newQqbs/28b0aaebfdcfc.png","width":207,"height":207,"url":"https://wap.myzebravip.com/lego/1569231035539.html?isHide=1&uid=100054","id":35452,"st":"2020-04-05T00:00:00.000+0800","et":"2038-01-19T11:14:07.000+0800","gcm":"2.1.1412f2052071000.e9f972377c23d863.203.0.26_35452."},{"title":"全国包邮","icon":"https://img.gegejia.com/newQqbs/1d5248dd47511.png","width":207,"height":207,"url":"https://wap.myzebravip.com/lego/1583299325239.html?isHide=1&uid=107003","id":27689,"st":"2020-04-05T00:00:00.000+0800","et":"2038-01-19T11:14:07.000+0800","gcm":"2.1.1412f2052071000.e9f972377c23d863.203.0.26_27689."},{"title":"旅游出行","icon":"https://img.gegejia.com/newQqbs/2e5fdbf4bda9e.png","width":207,"height":207,"url":"https://wap.myzebravip.com/lego/1585893336614.html?isHide=1&uid=108201","id":27690,"st":"2020-04-05T00:00:00.000+0800","et":"2038-01-19T11:14:07.000+0800","gcm":"2.1.1412f2052071000.e9f972377c23d863.203.0.26_27690."},{"title":"新会员","icon":"https://img.gegejia.com/newQqbs/28b16f5076fe6.png","width":207,"height":207,"url":"https://wap.myzebravip.com/lego/1578296140469.html?isHide=1&uid=105538","h5NotJump":false,"id":39775,"st":"2020-04-28T00:00:00.000+0800","et":"2038-01-19T11:14:07.000+0800","gcm":"2.1.1412f2052071000.e9f972377c23d863.203.0.26_39775."},{"title":"品牌制造","icon":"https://img.gegejia.com/newQqbs/28b0aaf3fa7a6.png","width":207,"height":207,"url":"https://wap.myzebravip.com/lego/1574063460759.html?isHide=1&uid=103173","id":27702,"st":"2020-04-05T00:00:00.000+0800","et":"2038-01-19T11:14:07.000+0800","gcm":"2.1.1412f2052071000.e9f972377c23d863.203.0.26_27702."},{"title":"斑马国际","icon":"https://img.gegejia.com/newQqbs/c44b5c598c5a.png","width":207,"height":207,"url":"https://wap.myzebravip.com/lego/1571301475233.html?isHide=1&uid=101609","id":27687,"st":"2020-04-05T00:00:00.000+0800","et":"2038-01-19T11:14:07.000+0800","gcm":"2.1.1412f2052071000.e9f972377c23d863.203.0.26_27687."},{"title":"1分钱试用","icon":"https://img.gegejia.com/newQqbs/69584c6991ae.png","width":207,"height":207,"url":"https://wap.myzebravip.com/trial/free?isHide=1","id":27703,"st":"2020-04-05T00:00:00.000+0800","et":"2038-01-19T11:14:07.000+0800","gcm":"2.1.1412f2052071000.e9f972377c23d863.203.0.26_27703."},{"title":"分类","icon":"https://img.gegejia.com/newQqbs/1d5248e572f8a.png","width":207,"height":207,"url":"https://wap.myzebravip.com/shop/classify","id":27704,"st":"2020-04-05T00:00:00.000+0800","et":"2038-01-19T11:14:07.000+0800","gcm":"2.1.1412f2052071000.e9f972377c23d863.203.0.26_27704."}]},{"code":"100128616589","key":"app_index_cust","list":[{"images":[{"image":"https://img.gegejia.com/newQqbs/11f4c4587a423.gif","itemImage":"https://img.gegejia.com/newQqbs/69661f1a4853.jpg","name":"image1","itemId":1294128,"rubanCode":200419605932,"url":"https://wap.myzebravip.com/lego/1586841870189.html?isHide=1&uid=108582&isHeaderFloat=1","width":1125,"height":360}],"style":"1","id":40909,"st":"2020-05-07T10:00:00.000+0800","et":"2020-05-09T23:59:59.000+0800","gcm":"2.1.1412efe5fc6f000.8e985ac44b39b00c.203.0.26_40909."},{"images":[{"image":"https://img.gegejia.com/newQqbs/c4592fa83465.jpg","itemImage":"https://img.gegejia.com/newQqbs/69661f422829.jpg","name":"image1","itemId":1567120,"rubanCode":200192127310,"url":"https://wap.myzebravip.com/lego/1587548298905.html?isHide=1&uid=108931","width":563,"height":276},{"image":"https://img.gegejia.com/newQqbs/17a3f50b2ace4.jpg","itemImage":"https://img.gegejia.com/newQqbs/11f4c40979079.png","name":"image2","itemId":0,"rubanCode":0,"url":"https://wap.myzebravip.com/lego/1587700723746.html?isHide=1&uid=109049","width":562,"height":276}],"style":"2","id":40914,"st":"2020-05-07T10:00:00.000+0800","et":"2020-05-09T23:59:59.000+0800","gcm":"2.1.1412efe6006f001.8e985ac44b39b00c.203.0.26_40914."},{"images":[{"image":"https://img.gegejia.com/newQqbs/340fea3843a8f.png","itemImage":"https://img.gegejia.com/newQqbs/696624aa0cc5.jpg","name":"image1","itemId":1562021,"rubanCode":200485432748,"url":"https://wap.myzebravip.com/lego/1587700745560.html?isHide=1&uid=109048","width":384,"height":360},{"image":"https://img.gegejia.com/newQqbs/11f4c40b6ede1.png","itemImage":"https://img.gegejia.com/newQqbs/e730f52ae5d.jpg","name":"image2","itemId":1100518,"rubanCode":200994357622,"url":"https://wap.myzebravip.com/lego/1587700682932.html?isHide=1&uid=109047","width":356,"height":360},{"image":"https://img.gegejia.com/newQqbs/17a3f5140fde3.png","itemImage":"https://img.gegejia.com/newQqbs/17a3f51514629.jpg","name":"image3","itemId":1383676,"rubanCode":200622267415,"url":"https://wap.myzebravip.com/lego/1586841988033.html?isHide=1&uid=108585","width":385,"height":360}],"style":"3","id":40916,"st":"2020-05-07T10:00:00.000+0800","et":"2020-05-08T09:59:59.000+0800","gcm":"2.1.1412efe6006f002.8e985ac44b39b00c.203.0.26_40916."},{"images":[{"image":"https://img.gegejia.com/newQqbs/c4593072ff62.png","itemImage":"https://img.gegejia.com/newQqbs/1d53266b4887c.jpg","name":"image1","itemId":1358088,"rubanCode":200393815516,"url":"https://wap.myzebravip.com/lego/1586841942010.html?isHide=1&uid=108586","width":384,"height":360},{"image":"https://img.gegejia.com/newQqbs/6966201a81f0.png","itemImage":"https://img.gegejia.com/newQqbs/c4593162a925.png","name":"image2","itemId":1628334,"rubanCode":201593293111,"url":"https://wap.myzebravip.com/lego/1581825962211.html?isHide=1&uid=106293","width":356,"height":360},{"image":"https://img.gegejia.com/newQqbs/69662072d31d.png","itemImage":"https://img.gegejia.com/newQqbs/28b188357e154.png","name":"image3","itemId":1638454,"rubanCode":201187934935,"url":"https://wap.myzebravip.com/lego/1587952869036.html?isHide=1&uid=109144","width":385,"height":360}],"style":"3","id":40917,"st":"2020-05-07T10:00:00.000+0800","et":"2020-05-08T09:59:59.000+0800","gcm":"2.1.1412efe6006f003.8e985ac44b39b00c.203.0.26_40917."},{"images":[{"image":"https://img.gegejia.com/newQqbs/69662088c8ce.gif","itemImage":"https://img.gegejia.com/newQqbs/c45931556eac.jpg","name":"image1","itemId":1102073,"rubanCode":201269394410,"url":"https://wap.myzebravip.com/lego/1587031379432.html?isHide=1&uid=108731","width":300,"height":330},{"image":"https://img.gegejia.com/newQqbs/11f4c417e12c1.png","itemImage":"https://img.gegejia.com/newQqbs/2e60b94362683.png","name":"image2","itemId":0,"rubanCode":0,"url":"https://wap.myzebravip.com/lego/1587036854581.html?isHide=1&uid=108736","width":263,"height":330},{"image":"https://img.gegejia.com/newQqbs/17a3f51f90efa.png","itemImage":"https://img.gegejia.com/newQqbs/1d53262c96af7.png","name":"image3","itemId":1032533,"rubanCode":200186468312,"url":"https://wap.myzebravip.com/lego/1588127635530.html?isHide=1&uid=109291","width":263,"height":330},{"image":"https://img.gegejia.com/newQqbs/1d53262743186.png","itemImage":"https://img.gegejia.com/newQqbs/28b1883c1e779.png","name":"image4","itemId":1280088,"rubanCode":201799222568,"url":"https://wap.myzebravip.com/lego/1587023545508.html?isHide=1&uid=108709","width":299,"height":330}],"style":"5","id":40924,"st":"2020-05-07T10:00:00.000+0800","et":"2020-05-08T23:59:59.000+0800","gcm":"2.1.1412efe6006f004.8e985ac44b39b00c.203.0.26_40924."},{"images":[{"image":"https://img.gegejia.com/newQqbs/e73114f6209.jpg","name":"image1","itemId":0,"rubanCode":0,"url":"https://wap.myzebravip.com/lego/1586841870189.html?isHide=1&uid=108582&isHeaderFloat=1","width":1125,"height":90}],"style":"0","id":40927,"st":"2020-05-07T10:00:00.000+0800","et":"2020-05-10T23:59:59.000+0800","gcm":"2.1.1412efe6046f000.8e985ac44b39b00c.203.0.26_40927."}]},{"code":"100893897145","key":"app_index_cust","list":[{"images":[{"image":"https://img.gegejia.com/newQqbs/e7322940b79.jpg","name":"image1","itemId":0,"rubanCode":0,"url":"https://wap.myzebravip.com/lego/1588213281668.html?isHide=1&uid=109374","width":560,"height":407},{"image":"https://img.gegejia.com/newQqbs/2e60ba6676765.jpg","name":"image2","itemId":0,"rubanCode":0,"url":"https://wap.myzebravip.com/lego/1588037270853.html?isHide=1&uid=109204","width":565,"height":407}],"style":"0","id":41069,"st":"2020-05-07T10:00:00.000+0800","et":"2020-05-08T09:59:59.000+0800","gcm":"2.1.1412f41cbc6e000.3264f2220c1c129d.203.0.26_41069."},{"images":[{"image":"https://img.gegejia.com/newQqbs/2302585a5a2df.jpg","name":"image1","itemId":0,"rubanCode":0,"url":"https://wap.myzebravip.com/lego/1581851298824.html?isHide=1&uid=106295","width":560,"height":400},{"image":"https://img.gegejia.com/newQqbs/c45961c61142.gif","name":"image2","itemId":0,"rubanCode":0,"url":"https://wap.myzebravip.com/discovery/index/index.html?selectTab=16","width":565,"height":400}],"style":"0","id":41075,"st":"2020-05-07T10:00:00.000+0800","et":"2020-05-07T13:59:59.000+0800","gcm":"2.1.1412f41cbc6e000.3264f2220c1c129d.203.0.26_41075."}]},{"code":"101803743826","key":"appIndexFlashSaleConfig","list":[{"type":"3","id":39102,"st":"2020-04-26T15:30:00.000+0800","et":"2038-01-19T11:14:07.000+0800","gcm":"2.1.1412edd25865000.14199c4c0a6f908c.203.0.26_39102."}]}]}
     */

    private DataBean data;

//    public DataBean getData() {
//        return data;
//    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        private List<DataListBean> dataList;

        public List<DataListBean> getDataList() {
            return dataList;
        }

        public void setDataList(List<DataListBean> dataList) {
            this.dataList = dataList;
        }

        public static class DataListBean {
            /**
             * code : 101105923976
             * key : appHomePageAtmosphere
             * list : [{"backColorIndex":"#f8f8f8","iconInnerColor":"#ffffff","iconTextColor":"#000000","scrollColor":"#1a1a1a","showIconScroll":true,"hotSelloptColor":"#ffffff","id":39134,"st":"2020-04-26T16:14:18.000+0800","et":"2038-01-19T11:14:07.000+0800","gcm":"2.1.1412f1bc2c78000.129d818dc94cd461.203.0.26_39134."}]
             */

            @SerializedName("code")
            private String codeX;
            private String key;
            private List<ListBean> list;

            public String getCodeX() {
                return codeX;
            }

            public void setCodeX(String codeX) {
                this.codeX = codeX;
            }

            public String getKey() {
                return key;
            }

            public void setKey(String key) {
                this.key = key;
            }

            public List<ListBean> getList() {
                return list;
            }

            public void setList(List<ListBean> list) {
                this.list = list;
            }

            public static class ListBean {
                /**
                 * backColorIndex : #f8f8f8
                 * iconInnerColor : #ffffff
                 * iconTextColor : #000000
                 * scrollColor : #1a1a1a
                 * showIconScroll : true
                 * hotSelloptColor : #ffffff
                 * id : 39134
                 * st : 2020-04-26T16:14:18.000+0800
                 * et : 2038-01-19T11:14:07.000+0800
                 * gcm : 2.1.1412f1bc2c78000.129d818dc94cd461.203.0.26_39134.
                 */

                private String backColorIndex;
                private String iconInnerColor;
                private String iconTextColor;
                private String scrollColor;
                private boolean showIconScroll;
                private String hotSelloptColor;
                private int id;
                private String st;
                private String et;
                private String gcm;

                public String getImage() {
                    return image;
                }

                public void setImage(String image) {
                    this.image = image;
                }

                public int getWidth() {
                    return width;
                }

                public void setWidth(int width) {
                    this.width = width;
                }

                public int getHeight() {
                    return height;
                }

                public void setHeight(int height) {
                    this.height = height;
                }

                public String getUrl() {
                    return url;
                }

                public void setUrl(String url) {
                    this.url = url;
                }

                public String getBgColor() {
                    return bgColor;
                }

                public void setBgColor(String bgColor) {
                    this.bgColor = bgColor;
                }

                public String getTitle() {
                    return title;
                }

                public void setTitle(String title) {
                    this.title = title;
                }

                public String getIcon() {
                    return icon;
                }

                public void setIcon(String icon) {
                    this.icon = icon;
                }

                public String getType() {
                    return type;
                }

                public void setType(String type) {
                    this.type = type;
                }

                private String image;
                private int width;
                private int height;
                private String url;
                private String bgColor;

                private String title;
                private String icon;
                private String type;


                public String getBackColorIndex() {
                    return backColorIndex;
                }

                public void setBackColorIndex(String backColorIndex) {
                    this.backColorIndex = backColorIndex;
                }

                public String getIconInnerColor() {
                    return iconInnerColor;
                }

                public void setIconInnerColor(String iconInnerColor) {
                    this.iconInnerColor = iconInnerColor;
                }

                public String getIconTextColor() {
                    return iconTextColor;
                }

                public void setIconTextColor(String iconTextColor) {
                    this.iconTextColor = iconTextColor;
                }

                public String getScrollColor() {
                    return scrollColor;
                }

                public void setScrollColor(String scrollColor) {
                    this.scrollColor = scrollColor;
                }

                public boolean isShowIconScroll() {
                    return showIconScroll;
                }

                public void setShowIconScroll(boolean showIconScroll) {
                    this.showIconScroll = showIconScroll;
                }

                public String getHotSelloptColor() {
                    return hotSelloptColor;
                }

                public void setHotSelloptColor(String hotSelloptColor) {
                    this.hotSelloptColor = hotSelloptColor;
                }

                public int getId() {
                    return id;
                }

                public void setId(int id) {
                    this.id = id;
                }

                public String getSt() {
                    return st;
                }

                public void setSt(String st) {
                    this.st = st;
                }

                public String getEt() {
                    return et;
                }

                public void setEt(String et) {
                    this.et = et;
                }

                public String getGcm() {
                    return gcm;
                }

                public void setGcm(String gcm) {
                    this.gcm = gcm;
                }
            }
        }
    }
}
