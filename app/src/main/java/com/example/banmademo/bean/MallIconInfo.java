package com.example.banmademo.bean;

import com.example.banmademo.base.BaseInfo;

/**
 * Create by kty
 * on 2020/5/8
 */
public class MallIconInfo extends BaseInfo {
    private String et;
    private String gcm;
    private boolean h5NotJump;
    private int height;
    private String icon;
    private int id;
    private String st;
    private String title;
    private String url;
    private int width;

    public String getEt() {
        return et;
    }

    public void setEt(String et) {
        this.et = et;
    }

    public String getGcm() {
        return gcm;
    }

    public void setGcm(String gcm) {
        this.gcm = gcm;
    }

    public boolean isH5NotJump() {
        return h5NotJump;
    }

    public void setH5NotJump(boolean h5NotJump) {
        this.h5NotJump = h5NotJump;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSt() {
        return st;
    }

    public void setSt(String st) {
        this.st = st;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }
}
