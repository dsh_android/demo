package com.example.banmademo.bean;

import com.example.banmademo.base.BaseInfo;

import java.util.List;

/**
 * Create by kty
 * on 2020/5/8
 */
public class MallCustomItemInfo extends BaseInfo {

    private String gcm;
    private int id;
    private List<ElementInfo> images;
    private String style;

    public String getGcm() {
        return gcm;
    }

    public void setGcm(String gcm) {
        this.gcm = gcm;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<ElementInfo> getImages() {
        return images;
    }

    public void setImages(List<ElementInfo> images) {
        this.images = images;
    }

    public String getStyle() {
        return style;
    }

    public void setStyle(String style) {
        this.style = style;
    }

    public static class ElementInfo {
        private int height;
        private String image;
        private String itemId;
        private String itemImage;
        private String name;
        private String rubanCode;
        private String url;
        private int width;

        public String getImage() {
            return this.image;
        }

        public void setImage(String str) {
            this.image = str;
        }

        public String getItemImage() {
            return this.itemImage;
        }

        public void setItemImage(String str) {
            this.itemImage = str;
        }

        public String getName() {
            return this.name;
        }

        public void setName(String str) {
            this.name = str;
        }

        public String getItemId() {
            return this.itemId;
        }

        public void setItemId(String str) {
            this.itemId = str;
        }

        public String getRubanCode() {
            return this.rubanCode;
        }

        public void setRubanCode(String str) {
            this.rubanCode = str;
        }

        public String getUrl() {
            return this.url;
        }

        public void setUrl(String str) {
            this.url = str;
        }

        public int getWidth() {
            return this.width;
        }

        public void setWidth(int i) {
            this.width = i;
        }

        public int getHeight() {
            return this.height;
        }

        public void setHeight(int i) {
            this.height = i;
        }
    }

}
