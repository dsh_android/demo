package com.example.banmademo.bean;

import com.example.banmademo.base.BaseInfo;

import java.util.List;

/**
 * Create by kty
 * on 2020/5/8
 */
public class MallIconResponse extends BaseInfo {

    private String code;
    private String key;
    private List<MallIconInfo> list;

    public String getCode() {
        String str = this.code;
        return str == null ? "" : str;
    }

    public void setCode(String str) {
        this.code = str;
    }

    public String getKey() {
        String str = this.key;
        return str == null ? "" : str;
    }

    public void setKey(String str) {
        this.key = str;
    }

    public List<MallIconInfo> getList() {
        return this.list;
    }

    public void setList(List<MallIconInfo> list2) {
        this.list = list2;
    }

}
