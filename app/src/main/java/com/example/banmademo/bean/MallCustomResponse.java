package com.example.banmademo.bean;

import com.example.banmademo.base.BaseInfo;

import java.util.List;

/**
 * Create by kty
 * on 2020/5/8
 */
public class MallCustomResponse extends BaseInfo {

    private String code;
    private String key;
    private List<MallCustomItemInfo> list;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public List<MallCustomItemInfo> getList() {
        return list;
    }

    public void setList(List<MallCustomItemInfo> list) {
        this.list = list;
    }
}
