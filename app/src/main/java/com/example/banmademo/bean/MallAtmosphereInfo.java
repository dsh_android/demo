package com.example.banmademo.bean;

import com.example.banmademo.base.BaseInfo;

/**
 * Create by kty
 * on 2020/5/8
 */
public class MallAtmosphereInfo extends BaseInfo {

    private String backColorBottom;
    private String backColorIndex;
    private String bannerBgImgBottom;
    private String bannerBgImgTop;
    private String et;
    private String gcm;
    private int height;
    private String hotSellHeadImg;
    private String hotSelloptColor;
    private String hotSelloptImg;
    private String iconBgImg;
    private String iconInnerColor;
    private String iconOutSideColor;
    private String iconRow;
    private String iconTextColor;
    private int id;
    private String scrollColor;
    private boolean showIconScroll;
    private String st;
    private int width;

    public String getBackColorBottom() {
        return backColorBottom;
    }

    public void setBackColorBottom(String backColorBottom) {
        this.backColorBottom = backColorBottom;
    }

    public String getBackColorIndex() {
        return backColorIndex;
    }

    public void setBackColorIndex(String backColorIndex) {
        this.backColorIndex = backColorIndex;
    }

    public String getBannerBgImgBottom() {
        return bannerBgImgBottom;
    }

    public void setBannerBgImgBottom(String bannerBgImgBottom) {
        this.bannerBgImgBottom = bannerBgImgBottom;
    }

    public String getBannerBgImgTop() {
        return bannerBgImgTop;
    }

    public void setBannerBgImgTop(String bannerBgImgTop) {
        this.bannerBgImgTop = bannerBgImgTop;
    }

    public String getEt() {
        return et;
    }

    public void setEt(String et) {
        this.et = et;
    }

    public String getGcm() {
        return gcm;
    }

    public void setGcm(String gcm) {
        this.gcm = gcm;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public String getHotSellHeadImg() {
        return hotSellHeadImg;
    }

    public void setHotSellHeadImg(String hotSellHeadImg) {
        this.hotSellHeadImg = hotSellHeadImg;
    }

    public String getHotSelloptColor() {
        return hotSelloptColor;
    }

    public void setHotSelloptColor(String hotSelloptColor) {
        this.hotSelloptColor = hotSelloptColor;
    }

    public String getHotSelloptImg() {
        return hotSelloptImg;
    }

    public void setHotSelloptImg(String hotSelloptImg) {
        this.hotSelloptImg = hotSelloptImg;
    }

    public String getIconBgImg() {
        return iconBgImg;
    }

    public void setIconBgImg(String iconBgImg) {
        this.iconBgImg = iconBgImg;
    }

    public String getIconInnerColor() {
        return iconInnerColor;
    }

    public void setIconInnerColor(String iconInnerColor) {
        this.iconInnerColor = iconInnerColor;
    }

    public String getIconOutSideColor() {
        return iconOutSideColor;
    }

    public void setIconOutSideColor(String iconOutSideColor) {
        this.iconOutSideColor = iconOutSideColor;
    }

    public String getIconRow() {
        return iconRow;
    }

    public void setIconRow(String iconRow) {
        this.iconRow = iconRow;
    }

    public String getIconTextColor() {
        return iconTextColor;
    }

    public void setIconTextColor(String iconTextColor) {
        this.iconTextColor = iconTextColor;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getScrollColor() {
        return scrollColor;
    }

    public void setScrollColor(String scrollColor) {
        this.scrollColor = scrollColor;
    }

    public boolean isShowIconScroll() {
        return showIconScroll;
    }

    public void setShowIconScroll(boolean showIconScroll) {
        this.showIconScroll = showIconScroll;
    }

    public String getSt() {
        return st;
    }

    public void setSt(String st) {
        this.st = st;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }
}
