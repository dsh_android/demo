package com.example.banmademo.bean;

import java.io.Serializable;
import java.util.List;

/**
 * Create by kty
 * on 2020/5/6
 */
public class ResultBaseEntity implements Serializable {


    /**
     * status : 1
     * code : 0
     * message :
     * data : {"dataList":[{"code":"101105923976","key":"appHomePageAtmosphere","list":[{"backColorIndex":"#f8f8f8","iconInnerColor":"#ffffff","iconTextColor":"#000000","scrollColor":"#1a1a1a","showIconScroll":true,"hotSelloptColor":"#ffffff","id":39134,"st":"2020-04-26T16:14:18.000+0800","et":"2038-01-19T11:14:07.000+0800","gcm":"2.1.143a9032c468000.e591c14da1611fa2.203.0.26_39134."}]},{"code":"100155957578","key":"appHomePageBanner","list":[{"image":"https://img.gegejia.com/newQqbs/c4593689b76d.jpg","width":690,"height":260,"url":"https://wap.myzebravip.com/lego/1588213724058.html?isHide=1&uid=109375","bgColor":"#337b8e","id":40949,"st":"2020-05-07T10:00:00.000+0800","et":"2020-05-09T09:59:59.000+0800","gcm":"2.1.143a8ccf546c002.6ddabe5d65d603b.203.0.26_40949."},{"image":"https://img.gegejia.com/newQqbs/696621057da0.jpg","width":690,"height":260,"url":"https://wap.myzebravip.com/lego/1588064484648.html?isHide=1&uid=109244","bgColor":"#a94b4b","id":39885,"st":"2020-05-06T10:00:00.000+0800","et":"2021-05-08T23:59:59.000+0800","gcm":"2.1.143a8ccf546c003.6ddabe5d65d603b.203.0.26_39885."},{"image":"https://img.gegejia.com/newQqbs/28b1890770f7b.jpg","width":690,"height":260,"url":"https://wap.myzebravip.com/lego/1588213281668.html?isHide=1&uid=109374","bgColor":"#447a6e","id":41026,"st":"2020-05-08T10:00:00.000+0800","et":"2020-05-09T09:59:59.000+0800","gcm":"2.1.143a8ccf546c004.6ddabe5d65d603b.203.0.26_41026."},{"image":"https://img.gegejia.com/newQqbs/1d532ce4954fe.png","width":690,"height":260,"url":"https://wap.myzebravip.com/lego/1588037270853.html?isHide=1&uid=109204","bgColor":"#557c38","id":28530,"st":"2020-05-08T10:00:00.000+0800","et":"2020-05-09T09:59:59.000+0800","gcm":"2.1.143a8ccf546c005.6ddabe5d65d603b.203.0.26_28530."},{"image":"https://img.gegejia.com/newQqbs/c459983e0a7d.jpg","width":690,"height":260,"url":"https://wap.myzebravip.com/lego/1587031379432.html?isHide=1&uid=108731","bgColor":"#641a16","id":41160,"st":"2020-05-08T10:00:00.000+0800","et":"2020-05-09T23:59:59.000+0800","gcm":"2.1.143a8ccf546c006.6ddabe5d65d603b.203.0.26_41160."},{"image":"https://img.gegejia.com/newQqbs/2e60bd463c5ac.jpg","width":690,"height":260,"url":"https://wap.myzebravip.com/lego/1588129943023.html?isHide=1&uid=109300","bgColor":"#77454e","id":41228,"st":"2020-05-08T10:00:00.000+0800","et":"2020-05-09T09:59:59.000+0800","gcm":"2.1.143a8ccf546c007.6ddabe5d65d603b.203.0.26_41228."},{"image":"https://img.gegejia.com/newQqbs/1d5329b680701.jpg","width":690,"height":260,"url":"https://wap.myzebravip.com/lego/1588127635530.html?isHide=1&uid=109291","bgColor":"#2d295c","id":41162,"st":"2020-05-08T10:00:00.000+0800","et":"2020-05-09T23:59:59.000+0800","gcm":"2.1.143a8ccf546c008.6ddabe5d65d603b.203.0.26_41162."}]},{"code":"100936418014","key":"appHomePageIcon","list":[{"title":"发现","icon":"https://img.gegejia.com/newQqbs/340f0cf6d3d71.png","width":207,"height":207,"url":"https://wap.myzebravip.com/discovery/index/","id":27678,"st":"2020-04-05T00:00:00.000+0800","et":"2038-01-19T11:14:07.000+0800","gcm":"2.1.143a8a06f879000.bdd4deadd9527b88.203.0.26_27678."},{"title":"新人红包","icon":"https://img.gegejia.com/newQqbs/17a31b05b3a8e.png","width":207,"height":207,"url":"https://wap.myzebravip.com/lego/1570498303615.html?isHide=1&uid=100743","id":27832,"st":"2020-04-11T00:00:00.000+0800","et":"2038-01-19T11:14:07.000+0800","gcm":"2.1.143a8a06f879000.bdd4deadd9527b88.203.0.26_27832."},{"title":"斑马出品","icon":"https://img.gegejia.com/newQqbs/230179e0c7149.png","width":207,"height":207,"url":"https://wap.myzebravip.com/lego/1573810926223.html?isHide=1&uid=103081","id":27681,"st":"2020-04-05T00:00:00.000+0800","et":"2038-01-19T11:14:07.000+0800","gcm":"2.1.143a8a06f879000.bdd4deadd9527b88.203.0.26_27681."},{"title":"积分商品","icon":"https://img.gegejia.com/newQqbs/1d5248d9c93c7.gif","width":207,"height":207,"url":"https://wap.myzebravip.com/buyer_lego/lego/1582538393644.html?isHide=1&uid=106627","h5NotJump":false,"id":27682,"st":"2020-04-05T00:00:00.000+0800","et":"2038-01-19T11:14:07.000+0800","gcm":"2.1.143a8a06f879000.bdd4deadd9527b88.203.0.26_27682."},{"title":"中国田","icon":"https://img.gegejia.com/newQqbs/69584bbfa5a0.png","width":207,"height":207,"url":"https://wap.myzebravip.com/lego/1575509094636.html?isHide=1&uid=104150","id":27684,"st":"2020-04-05T00:00:00.000+0800","et":"2038-01-19T11:14:07.000+0800","gcm":"2.1.143a8a06f879000.bdd4deadd9527b88.203.0.26_27684."},{"title":"斑马优选","icon":"https://img.gegejia.com/newQqbs/17a317d3a5bc8.gif","width":207,"height":207,"url":"https://wap.myzebravip.com/lego/1588045165385.html?isHide=1&uid=109219","id":27685,"st":"2020-04-05T00:00:00.000+0800","et":"2038-01-19T11:14:07.000+0800","gcm":"2.1.143a8a06f879000.bdd4deadd9527b88.203.0.26_27685."},{"title":"斑马果园","icon":"https://img.gegejia.com/newQqbs/17a317d42ccc9.png","width":207,"height":207,"url":"https://wap.myzebravip.com/orchard/","id":27686,"st":"2020-04-05T00:00:00.000+0800","et":"2038-01-19T11:14:07.000+0800","gcm":"2.1.143a8a06f879000.bdd4deadd9527b88.203.0.26_27686."},{"title":"斑马CBT","icon":"https://img.gegejia.com/newQqbs/28b0e85083db7.png","width":207,"height":207,"url":"https://x.myzebravip.com/lego/1585303324372.html?isHide=1&uid=107841","id":27458,"st":"2020-04-05T00:00:00.000+0800","et":"2038-01-19T11:14:07.000+0800","gcm":"2.1.143a8a06f879000.bdd4deadd9527b88.203.0.26_27458."},{"title":"热销榜单","icon":"https://img.gegejia.com/newQqbs/28b0aaebfdcfc.png","width":207,"height":207,"url":"https://wap.myzebravip.com/lego/1569231035539.html?isHide=1&uid=100054","id":35452,"st":"2020-04-05T00:00:00.000+0800","et":"2038-01-19T11:14:07.000+0800","gcm":"2.1.143a8a06f879000.bdd4deadd9527b88.203.0.26_35452."},{"title":"全国包邮","icon":"https://img.gegejia.com/newQqbs/1d5248dd47511.png","width":207,"height":207,"url":"https://wap.myzebravip.com/lego/1583299325239.html?isHide=1&uid=107003","id":27689,"st":"2020-04-05T00:00:00.000+0800","et":"2038-01-19T11:14:07.000+0800","gcm":"2.1.143a8a06f879000.bdd4deadd9527b88.203.0.26_27689."},{"title":"旅游出行","icon":"https://img.gegejia.com/newQqbs/2e5fdbf4bda9e.png","width":207,"height":207,"url":"https://wap.myzebravip.com/lego/1585893336614.html?isHide=1&uid=108201","id":27690,"st":"2020-04-05T00:00:00.000+0800","et":"2038-01-19T11:14:07.000+0800","gcm":"2.1.143a8a06f879000.bdd4deadd9527b88.203.0.26_27690."},{"title":"新会员","icon":"https://img.gegejia.com/newQqbs/28b16f5076fe6.png","width":207,"height":207,"url":"https://wap.myzebravip.com/lego/1578296140469.html?isHide=1&uid=105538","h5NotJump":false,"id":39775,"st":"2020-04-28T00:00:00.000+0800","et":"2038-01-19T11:14:07.000+0800","gcm":"2.1.143a8a06f879000.bdd4deadd9527b88.203.0.26_39775."},{"title":"品牌制造","icon":"https://img.gegejia.com/newQqbs/28b0aaf3fa7a6.png","width":207,"height":207,"url":"https://wap.myzebravip.com/lego/1574063460759.html?isHide=1&uid=103173","id":27702,"st":"2020-04-05T00:00:00.000+0800","et":"2038-01-19T11:14:07.000+0800","gcm":"2.1.143a8a06f879000.bdd4deadd9527b88.203.0.26_27702."},{"title":"斑马国际","icon":"https://img.gegejia.com/newQqbs/c44b5c598c5a.png","width":207,"height":207,"url":"https://wap.myzebravip.com/lego/1571301475233.html?isHide=1&uid=101609","id":27687,"st":"2020-04-05T00:00:00.000+0800","et":"2038-01-19T11:14:07.000+0800","gcm":"2.1.143a8a06f879000.bdd4deadd9527b88.203.0.26_27687."},{"title":"1分钱试用","icon":"https://img.gegejia.com/newQqbs/69584c6991ae.png","width":207,"height":207,"url":"https://wap.myzebravip.com/trial/free?isHide=1","id":27703,"st":"2020-04-05T00:00:00.000+0800","et":"2038-01-19T11:14:07.000+0800","gcm":"2.1.143a8a06f879000.bdd4deadd9527b88.203.0.26_27703."},{"title":"分类","icon":"https://img.gegejia.com/newQqbs/1d5248e572f8a.png","width":207,"height":207,"url":"https://wap.myzebravip.com/shop/classify","id":27704,"st":"2020-04-05T00:00:00.000+0800","et":"2038-01-19T11:14:07.000+0800","gcm":"2.1.143a8a06f879000.bdd4deadd9527b88.203.0.26_27704."}]},{"code":"100128616589","key":"app_index_cust","list":[{"images":[{"image":"https://img.gegejia.com/newQqbs/11f4c4587a423.gif","itemImage":"https://img.gegejia.com/newQqbs/69661f1a4853.jpg","name":"image1","itemId":1294128,"rubanCode":200419605932,"url":"https://wap.myzebravip.com/lego/1586841870189.html?isHide=1&uid=108582&isHeaderFloat=1","width":1125,"height":360}],"style":"1","id":40909,"st":"2020-05-07T10:00:00.000+0800","et":"2020-05-09T23:59:59.000+0800","gcm":"2.1.143a8f8c3470000.e61030d3fe5fe065.203.0.26_40909."},{"images":[{"image":"https://img.gegejia.com/newQqbs/c4592fa83465.jpg","itemImage":"https://img.gegejia.com/newQqbs/69661f422829.jpg","name":"image1","itemId":1567120,"rubanCode":200192127310,"url":"https://wap.myzebravip.com/lego/1587548298905.html?isHide=1&uid=108931","width":563,"height":276},{"image":"https://img.gegejia.com/newQqbs/17a3f50b2ace4.jpg","itemImage":"https://img.gegejia.com/newQqbs/11f4c40979079.png","name":"image2","itemId":0,"rubanCode":0,"url":"https://wap.myzebravip.com/lego/1587700723746.html?isHide=1&uid=109049","width":562,"height":276}],"style":"2","id":40914,"st":"2020-05-07T10:00:00.000+0800","et":"2020-05-09T23:59:59.000+0800","gcm":"2.1.143a8f8c3470000.e61030d3fe5fe065.203.0.26_40914."},{"images":[{"image":"https://img.gegejia.com/newQqbs/c45971120cfa.jpg","itemImage":"https://img.gegejia.com/newQqbs/696624aa0cc5.jpg","name":"image1","itemId":1562021,"rubanCode":200485432748,"url":"https://wap.myzebravip.com/lego/1587700745560.html?isHide=1&uid=109048","width":563,"height":276},{"image":"https://img.gegejia.com/newQqbs/11f4c818d7d9b.jpg","itemImage":"https://img.gegejia.com/newQqbs/e730f52ae5d.jpg","name":"image2","itemId":1100518,"rubanCode":200994357622,"url":"https://wap.myzebravip.com/lego/1587700682932.html?isHide=1&uid=109047","width":562,"height":276}],"style":"2","id":41220,"st":"2020-05-08T10:00:00.000+0800","et":"2020-05-09T09:59:59.000+0800","gcm":"2.1.143a8f8c3470000.e61030d3fe5fe065.203.0.26_41220."},{"images":[{"image":"https://img.gegejia.com/newQqbs/340fee4764eb5.jpg","itemImage":"https://img.gegejia.com/newQqbs/696660ce3f08.png","name":"image1","itemId":1073636,"rubanCode":201169611441,"url":"https://wap.myzebravip.com/lego/1587019346126.html?isHide=1&uid=108701","width":300,"height":330},{"image":"https://img.gegejia.com/newQqbs/2e60bd3fe7ad1.jpg","itemImage":"https://img.gegejia.com/newQqbs/c45972196476.jpg","name":"image2","itemId":1159393,"rubanCode":200654219424,"url":"https://wap.myzebravip.com/lego/1587368500310.html?isHide=1&uid=108849","width":263,"height":330},{"image":"https://img.gegejia.com/newQqbs/11f4cd7b7a565.jpg","itemImage":"https://img.gegejia.com/newQqbs/23025b4097ba9.png","name":"image3","itemId":1133402,"rubanCode":201005404032,"url":"https://wap.myzebravip.com/lego/1587021198698.html?isHide=1&uid=108705","width":263,"height":330},{"image":"https://img.gegejia.com/newQqbs/11f4c819f02d4.jpg","itemImage":"https://img.gegejia.com/newQqbs/e73512f0537.png","name":"image4","itemId":1034179,"rubanCode":201689725355,"url":"https://wap.myzebravip.com/lego/1587016601692.html?isHide=1&uid=108698","width":299,"height":330}],"style":"5","id":41222,"st":"2020-05-08T10:00:00.000+0800","et":"2020-05-09T09:59:59.000+0800","gcm":"2.1.143a8f8c3470000.e61030d3fe5fe065.203.0.26_41222."},{"images":[{"image":"https://img.gegejia.com/newQqbs/17a3f9324d89b.jpg","itemImage":"https://img.gegejia.com/newQqbs/17a3f932e4e12.png","name":"image1","itemId":1681585,"rubanCode":200381420991,"url":"https://wap.myzebravip.com/lego/1587022925508.html?isHide=1&uid=108706","width":300,"height":330},{"image":"https://img.gegejia.com/newQqbs/2e60bd50eb0e5.jpg","itemImage":"https://img.gegejia.com/newQqbs/28b18c4b01ac3.jpg","name":"image2","itemId":1016875,"rubanCode":201792123689,"url":"https://wap.myzebravip.com/lego/1587883480105.html?isHide=1&uid=109106","width":263,"height":330},{"image":"https://img.gegejia.com/newQqbs/e73513d7585.jpg","itemImage":"https://img.gegejia.com/newQqbs/696661d5e4cf.png","name":"image3","itemId":1023987,"rubanCode":201192443963,"url":"https://wap.myzebravip.com/lego/1587029116196.html?isHide=1&uid=108722","width":263,"height":330},{"image":"https://img.gegejia.com/newQqbs/17a3f93277499.jpg","itemImage":"https://img.gegejia.com/newQqbs/28b18c4bcbd26.png","name":"image4","itemId":1117013,"rubanCode":201102500481,"url":"https://wap.myzebravip.com/lego/1587028882518.html?isHide=1&uid=108717","width":299,"height":330}],"style":"5","id":41238,"st":"2020-05-08T10:00:00.000+0800","et":"2020-05-09T09:59:59.000+0800","gcm":"2.1.143a8f8c3470000.e61030d3fe5fe065.203.0.26_41238."},{"images":[{"image":"https://img.gegejia.com/newQqbs/e73114f6209.jpg","name":"image1","itemId":0,"rubanCode":0,"url":"https://wap.myzebravip.com/lego/1586841870189.html?isHide=1&uid=108582&isHeaderFloat=1","width":1125,"height":90}],"style":"0","id":40927,"st":"2020-05-07T10:00:00.000+0800","et":"2020-05-10T23:59:59.000+0800","gcm":"2.1.143a8f8c3470000.e61030d3fe5fe065.203.0.26_40927."}]},{"code":"100893897145","key":"app_index_cust","list":[{"images":[{"image":"https://img.gegejia.com/newQqbs/28b18c584f374.jpg","name":"image1","itemId":0,"rubanCode":0,"url":"https://wap.myzebravip.com/lego/1588749536748.html?isHide=1&uid=109448","width":560,"height":407},{"image":"https://img.gegejia.com/newQqbs/11f4caedfb6db.jpg","name":"image2","itemId":0,"rubanCode":0,"url":"https://wap.myzebravip.com/lego/1588646323591.html?isHide=1&uid=109421","width":565,"height":407}],"style":"0","id":41244,"st":"2020-05-08T10:00:00.000+0800","et":"2020-05-09T23:59:59.000+0800","gcm":"2.1.143a8fb46074000.afc2dea1b93f19c4.203.0.26_41244."},{"images":[{"image":"https://img.gegejia.com/newQqbs/1d532a365b828.jpg","name":"image1","itemId":0,"rubanCode":0,"url":"https://wap.myzebravip.com/lego/1581851298824.html?isHide=1&uid=106295","width":560,"height":400},{"image":"https://img.gegejia.com/newQqbs/1d532dab81983.gif","name":"image2","itemId":0,"rubanCode":0,"url":"https://wap.myzebravip.com/discovery/index/index.html?selectTab=16","width":565,"height":400}],"style":"0","id":41397,"st":"2020-05-09T00:00:00.000+0800","et":"2020-05-09T09:59:59.000+0800","gcm":"2.1.143a8fb46074000.afc2dea1b93f19c4.203.0.26_41397."}]},{"code":"101803743826","key":"appIndexFlashSaleConfig","list":[{"type":"3","id":39102,"st":"2020-04-26T15:30:00.000+0800","et":"2038-01-19T11:14:07.000+0800","gcm":"2.1.143a8fb46074000.afc2dea1b93f19c4.203.0.26_39102."}]}]}
     */

    private int status;
    private int code;
    private String message;
    private DataBean data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        private List<DataListBean> dataList;

        public List<DataListBean> getDataList() {
            return dataList;
        }

        public void setDataList(List<DataListBean> dataList) {
            this.dataList = dataList;
        }

        public static class DataListBean {
            /**
             * code : 101105923976
             * key : appHomePageAtmosphere
             * list : [{"backColorIndex":"#f8f8f8","iconInnerColor":"#ffffff","iconTextColor":"#000000","scrollColor":"#1a1a1a","showIconScroll":true,"hotSelloptColor":"#ffffff","id":39134,"st":"2020-04-26T16:14:18.000+0800","et":"2038-01-19T11:14:07.000+0800","gcm":"2.1.143a9032c468000.e591c14da1611fa2.203.0.26_39134."}]
             */

            private String code;
            private String key;
            private List<ListBean> list;

            public String getCode() {
                return code;
            }

            public void setCode(String code) {
                this.code = code;
            }

            public String getKey() {
                return key;
            }

            public void setKey(String key) {
                this.key = key;
            }

            public List<ListBean> getList() {
                return list;
            }

            public void setList(List<ListBean> list) {
                this.list = list;
            }

            public static class ListBean {
                /**
                 * backColorIndex : #f8f8f8
                 * iconInnerColor : #ffffff
                 * iconTextColor : #000000
                 * scrollColor : #1a1a1a
                 * showIconScroll : true
                 * hotSelloptColor : #ffffff
                 * id : 39134
                 * st : 2020-04-26T16:14:18.000+0800
                 * et : 2038-01-19T11:14:07.000+0800
                 * gcm : 2.1.143a9032c468000.e591c14da1611fa2.203.0.26_39134.
                 */

                private String backColorIndex;
                private String iconInnerColor;
                private String iconTextColor;
                private String scrollColor;
                private boolean showIconScroll;
                private String hotSelloptColor;
                private int id;
                private String st;
                private String et;
                private String gcm;

                public String getBackColorIndex() {
                    return backColorIndex;
                }

                public void setBackColorIndex(String backColorIndex) {
                    this.backColorIndex = backColorIndex;
                }

                public String getIconInnerColor() {
                    return iconInnerColor;
                }

                public void setIconInnerColor(String iconInnerColor) {
                    this.iconInnerColor = iconInnerColor;
                }

                public String getIconTextColor() {
                    return iconTextColor;
                }

                public void setIconTextColor(String iconTextColor) {
                    this.iconTextColor = iconTextColor;
                }

                public String getScrollColor() {
                    return scrollColor;
                }

                public void setScrollColor(String scrollColor) {
                    this.scrollColor = scrollColor;
                }

                public boolean isShowIconScroll() {
                    return showIconScroll;
                }

                public void setShowIconScroll(boolean showIconScroll) {
                    this.showIconScroll = showIconScroll;
                }

                public String getHotSelloptColor() {
                    return hotSelloptColor;
                }

                public void setHotSelloptColor(String hotSelloptColor) {
                    this.hotSelloptColor = hotSelloptColor;
                }

                public int getId() {
                    return id;
                }

                public void setId(int id) {
                    this.id = id;
                }

                public String getSt() {
                    return st;
                }

                public void setSt(String st) {
                    this.st = st;
                }

                public String getEt() {
                    return et;
                }

                public void setEt(String et) {
                    this.et = et;
                }

                public String getGcm() {
                    return gcm;
                }

                public void setGcm(String gcm) {
                    this.gcm = gcm;
                }
            }
        }
    }
}
