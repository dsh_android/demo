package com.example.banmademo.bean;

import com.example.banmademo.base.BaseInfo;

import java.util.List;

/**
 * Create by kty
 * on 2020/5/8
 */
public class TodayRecommendInfo extends BaseInfo {

    private String gcm;
    private int height;
    private int id;
    private String image;
    private List<ProductInfo> items;
    private String url;
    private int width;

    public String getGcm() {
        return gcm;
    }

    public void setGcm(String gcm) {
        this.gcm = gcm;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public List<ProductInfo> getItems() {
        return items;
    }

    public void setItems(List<ProductInfo> items) {
        this.items = items;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public static class ProductInfo extends BaseInfo {
        private String brokerageStr;
        private String imageUrl;

        public String getImageUrl() {
            String str = this.imageUrl;
            return str == null ? "" : str;
        }

        public void setImageUrl(String str) {
            this.imageUrl = str;
        }

        public String getBrokerageStr() {
            String str = this.brokerageStr;
            return str == null ? "" : str;
        }

        public void setBrokerageStr(String str) {
            this.brokerageStr = str;
        }
    }
}
