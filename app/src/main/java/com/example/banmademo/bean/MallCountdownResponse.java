package com.example.banmademo.bean;

import com.example.banmademo.base.BaseInfo;

import java.util.List;

/**
 * Create by kty
 * on 2020/5/8
 */
public class MallCountdownResponse extends BaseInfo {

    private String code;
    private String key;
    private List<MallCountdownInfo> list;

    public String getCode() {
        return this.code;
    }

    public void setCode(String str) {
        this.code = str;
    }

    public String getKey() {
        return this.key;
    }

    public void setKey(String str) {
        this.key = str;
    }

    public List<MallCountdownInfo> getList() {
        return this.list;
    }

    public void setList(List<MallCountdownInfo> list2) {
        this.list = list2;
    }

}
