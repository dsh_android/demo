package com.example.banmademo.bean;

import com.example.banmademo.base.BaseInfo;

/**
 * Create by kty
 * on 2020/5/8
 */
public class MallCountdownInfo extends BaseInfo {

    private String backImage;
    private long expireSeconds;
    private String gcm;
    private int height;
    private int id;
    private String timeBackColor;
    private String timeTextColor;
    private String tip;
    private String tipColor;
    private String url;
    private int width;

    public String getBackImage() {
        return backImage;
    }

    public void setBackImage(String backImage) {
        this.backImage = backImage;
    }

    public long getExpireSeconds() {
        return expireSeconds;
    }

    public void setExpireSeconds(long expireSeconds) {
        this.expireSeconds = expireSeconds;
    }

    public String getGcm() {
        return gcm;
    }

    public void setGcm(String gcm) {
        this.gcm = gcm;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTimeBackColor() {
        return timeBackColor;
    }

    public void setTimeBackColor(String timeBackColor) {
        this.timeBackColor = timeBackColor;
    }

    public String getTimeTextColor() {
        return timeTextColor;
    }

    public void setTimeTextColor(String timeTextColor) {
        this.timeTextColor = timeTextColor;
    }

    public String getTip() {
        return tip;
    }

    public void setTip(String tip) {
        this.tip = tip;
    }

    public String getTipColor() {
        return tipColor;
    }

    public void setTipColor(String tipColor) {
        this.tipColor = tipColor;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }
}
