package com.example.banmademo.bean;

import com.example.banmademo.base.BaseInfo;

import java.util.List;

/**
 * Create by kty
 * on 2020/5/8
 */
public class MallAtmosphereResponse extends BaseInfo {

    private String code;
    private String key;
    private List<MallAtmosphereInfo> list;

    public String getCode() {
        return this.code;
    }

    public void setCode(String str) {
        this.code = str;
    }

    public String getKey() {
        return this.key;
    }

    public void setKey(String str) {
        this.key = str;
    }

    public List<MallAtmosphereInfo> getList() {
        return this.list;
    }

    public void setList(List<MallAtmosphereInfo> list2) {
        this.list = list2;
    }

}
