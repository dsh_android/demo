package com.example.banmademo.utils.glide;

/**
 * Create by kty
 * on 2020/5/9
 */

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;

import androidx.annotation.WorkerThread;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestBuilder;
import com.bumptech.glide.TransitionOptions;
import com.bumptech.glide.request.FutureTarget;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;

import java.io.File;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import kotlin.jvm.internal.Intrinsics;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class GlideHelper {
    public static final GlideHelper INSTANCE = new GlideHelper();

    private GlideHelper() {
    }

    public final void loadUrl(@Nullable String str, @Nullable ImageView imageView) {
        if (imageView != null) {
            Context context = imageView.getContext();
            Intrinsics.checkExpressionValueIsNotNull(context, "imageView.context");
            loadUrl(context, str, (RequestOptions) null, imageView);
        }
    }

    public static /* synthetic */ void loadUrl$default(GlideHelper glideHelper, String str, RequestOptions requestOptions, ImageView imageView, int i, Object obj) {
        if ((i & 2) != 0) {
            requestOptions = null;
        }
        glideHelper.loadUrl(str, requestOptions, imageView);
    }

    public final void loadUrl(@Nullable String str, @Nullable RequestOptions requestOptions, @Nullable ImageView imageView) {
        if (imageView != null) {
            Context context = imageView.getContext();
            Intrinsics.checkExpressionValueIsNotNull(context, "imageView.context");
            loadUrl(context, str, requestOptions, imageView);
        }
    }

    public final void loadUrl(@NotNull Context context, @Nullable String str, @Nullable ImageView imageView) {
        loadUrl(context, str, (RequestOptions) null, imageView);
    }

    public static /* synthetic */ void loadUrl$default(GlideHelper glideHelper, Context context, String str, RequestOptions requestOptions, ImageView imageView, int i, Object obj) {
        if ((i & 4) != 0) {
            requestOptions = null;
        }
        glideHelper.loadUrl(context, str, requestOptions, imageView);
    }

    public final void loadUrl(@NotNull Context context, @Nullable String str, @Nullable RequestOptions requestOptions, @Nullable ImageView imageView) {
        if (imageView != null && !isDestroy(context)) {
            RequestBuilder load = Glide.with(context).load(str);
            Intrinsics.checkExpressionValueIsNotNull(load, "Glide.with(context).load(url)");
            if (requestOptions != null) {
                load.apply(requestOptions);
            }
            load.into(imageView);
        }
    }

    public static /* synthetic */ void loadUrl$default(GlideHelper glideHelper, Context context, String str, RequestOptions requestOptions, TransitionOptions transitionOptions, ImageView imageView, int i, Object obj) {
        RequestOptions requestOptions2 = (i & 4) != 0 ? null : requestOptions;
        if ((i & 8) != 0) {
            transitionOptions = null;
        }
        glideHelper.loadUrl(context, str, requestOptions2, transitionOptions, imageView);
    }

    public final void loadUrl(@NotNull Context context, @Nullable String str, @Nullable RequestOptions requestOptions, @Nullable TransitionOptions<?, Drawable> transitionOptions, @Nullable ImageView imageView) {
        if (imageView != null && !isDestroy(context)) {
            RequestBuilder load = Glide.with(context).load(str);
            Intrinsics.checkExpressionValueIsNotNull(load, "Glide.with(context).load(url)");
            if (requestOptions != null) {
                load.apply(requestOptions);
            }
            if (transitionOptions != null) {
                load.transition(transitionOptions);
            }
            load.into(imageView);
        }
    }

    public static /* synthetic */ void loadUrl$default(GlideHelper glideHelper, Context context, Bitmap bitmap, RequestOptions requestOptions, ImageView imageView, int i, Object obj) {
        if ((i & 4) != 0) {
            requestOptions = null;
        }
        glideHelper.loadUrl(context, bitmap, requestOptions, imageView);
    }

    public final void loadUrl(@NotNull Context context, @Nullable Bitmap bitmap, @Nullable RequestOptions requestOptions, @Nullable ImageView imageView) {
        if (imageView != null && !isDestroy(context) && bitmap != null && !bitmap.isRecycled()) {
            RequestBuilder load = Glide.with(context).load(bitmap);
            Intrinsics.checkExpressionValueIsNotNull(load, "Glide.with(context).load(bitmap)");
            if (requestOptions != null) {
                load.apply(requestOptions);
            }
            load.into(imageView);
        }
    }

    public final void loadRes(int i, @Nullable ImageView imageView) {
        if (imageView != null) {
            Context context = imageView.getContext();
            Intrinsics.checkExpressionValueIsNotNull(context, "imageView.context");
            loadRes(context, i, (RequestOptions) null, imageView);
        }
    }

    public static /* synthetic */ void loadRes$default(GlideHelper glideHelper, int i, RequestOptions requestOptions, ImageView imageView, int i2, Object obj) {
        if ((i2 & 2) != 0) {
            requestOptions = null;
        }
        glideHelper.loadRes(i, requestOptions, imageView);
    }

    public final void loadRes(int i, @Nullable RequestOptions requestOptions, @Nullable ImageView imageView) {
        if (imageView != null) {
            Context context = imageView.getContext();
            Intrinsics.checkExpressionValueIsNotNull(context, "imageView.context");
            loadRes(context, i, requestOptions, imageView);
        }
    }

    public final void loadRes(@NotNull Context context, int i, @Nullable ImageView imageView) {
        loadRes(context, i, (RequestOptions) null, imageView);
    }

    public static /* synthetic */ void loadRes$default(GlideHelper glideHelper, Context context, int i, RequestOptions requestOptions, ImageView imageView, int i2, Object obj) {
        if ((i2 & 4) != 0) {
            requestOptions = null;
        }
        glideHelper.loadRes(context, i, requestOptions, imageView);
    }

    public final void loadRes(@NotNull Context context, int i, @Nullable RequestOptions requestOptions, @Nullable ImageView imageView) {
        if (imageView != null && !isDestroy(context)) {
            RequestBuilder load = Glide.with(context).load(Integer.valueOf(i));
            Intrinsics.checkExpressionValueIsNotNull(load, "Glide.with(context).load(resId)");
            if (requestOptions != null) {
                load.apply(requestOptions);
            }
            load.into(imageView);
        }
    }

    public static /* synthetic */ void loadUrl$default(GlideHelper glideHelper, Context context, String str, RequestOptions requestOptions, Target target, int i, Object obj) {
        if ((i & 4) != 0) {
            requestOptions = null;
        }
        glideHelper.loadUrl(context, str, requestOptions, (Target<Drawable>) target);
    }

    public final void loadUrl(@NotNull Context context, @Nullable String str, @Nullable RequestOptions requestOptions, @Nullable Target<Drawable> target) {
        if (target != null && !isDestroy(context)) {
            RequestBuilder load = Glide.with(context).load(str);
            Intrinsics.checkExpressionValueIsNotNull(load, "Glide.with(context).load(url)");
            if (requestOptions != null) {
                load.apply(requestOptions);
            }
            load.into(target);
        }
    }

    public static void loadUrlDefault(GlideHelper glideHelper, Fragment fragment, String str, RequestOptions requestOptions, ImageView imageView, int i, Object obj) {
        if ((i & 4) != 0) {
            requestOptions = null;
        }
        glideHelper.loadUrl(fragment, str, requestOptions, imageView);
    }

    public final void loadUrl(@NotNull Fragment fragment, @Nullable String str, @Nullable RequestOptions requestOptions, @Nullable ImageView imageView) {
        if (imageView != null) {
            RequestBuilder load = Glide.with(fragment).load(str);
            Intrinsics.checkExpressionValueIsNotNull(load, "Glide.with(fragment).load(url)");
            if (requestOptions != null) {
                load.apply(requestOptions);
            }
            load.into(imageView);
        }
    }

    public static /* synthetic */ void loadUrl$default(GlideHelper glideHelper, Fragment fragment, String str, RequestOptions requestOptions, Target target, int i, Object obj) {
        if ((i & 4) != 0) {
            requestOptions = null;
        }
        glideHelper.loadUrl(fragment, str, requestOptions, (Target<Drawable>) target);
    }

    public final void loadUrl(@NotNull Fragment fragment, @Nullable String str, @Nullable RequestOptions requestOptions, @Nullable Target<Drawable> target) {
        if (target != null) {
            RequestBuilder load = Glide.with(fragment).load(str);
            Intrinsics.checkExpressionValueIsNotNull(load, "Glide.with(fragment).load(url)");
            if (requestOptions != null) {
                load.apply(requestOptions);
            }
            load.into(target);
        }
    }

    public final void loadGif(@NotNull Context context, @Nullable String str, @Nullable ImageView imageView) {
        loadGif(context, str, (RequestOptions) null, imageView);
    }

    public static /* synthetic */ void loadGif$default(GlideHelper glideHelper, Context context, String str, RequestOptions requestOptions, ImageView imageView, int i, Object obj) {
        if ((i & 4) != 0) {
            requestOptions = null;
        }
        glideHelper.loadGif(context, str, requestOptions, imageView);
    }

    public final void loadGif(@NotNull Context context, @Nullable String str, @Nullable RequestOptions requestOptions, @Nullable ImageView imageView) {
        if (imageView != null && !isDestroy(context)) {
            RequestBuilder load = Glide.with(context).asGif().load(str);
            Intrinsics.checkExpressionValueIsNotNull(load, "Glide.with(context)\n    …               .load(url)");
            if (requestOptions != null) {
                load.apply(requestOptions);
            }
            load.into(imageView);
        }
    }

    public final void loadGif(@NotNull Context context, @Nullable File file, @Nullable ImageView imageView) {
        loadGif(context, file, (RequestOptions) null, imageView);
    }

    public static /* synthetic */ void loadGif$default(GlideHelper glideHelper, Context context, File file, RequestOptions requestOptions, ImageView imageView, int i, Object obj) {
        if ((i & 4) != 0) {
            requestOptions = null;
        }
        glideHelper.loadGif(context, file, requestOptions, imageView);
    }

    public final void loadGif(@NotNull Context context, @Nullable File file, @Nullable RequestOptions requestOptions, @Nullable ImageView imageView) {
        if (imageView != null && !isDestroy(context)) {
            RequestBuilder load = Glide.with(context).asGif().load(file);
            Intrinsics.checkExpressionValueIsNotNull(load, "Glide.with(context)\n    …           .load(gifFile)");
            if (requestOptions != null) {
                load.apply(requestOptions);
            }
            load.into(imageView);
        }
    }

    public final void download(@NotNull Context context, @Nullable String str, @Nullable Target<Bitmap> target) {
        download(context, str, (RequestOptions) null, target);
    }

    public static /* synthetic */ void download$default(GlideHelper glideHelper, Context context, String str, RequestOptions requestOptions, Target target, int i, Object obj) {
        if ((i & 4) != 0) {
            requestOptions = null;
        }
        glideHelper.download(context, str, requestOptions, target);
    }

    public final void download(@NotNull Context context, @Nullable String str, @Nullable RequestOptions requestOptions, @Nullable Target<Bitmap> target) {
        if (target != null && !isDestroy(context)) {
            RequestBuilder load = Glide.with(context).asBitmap().load(str);
            Intrinsics.checkExpressionValueIsNotNull(load, "Glide.with(context)\n    …               .load(url)");
            if (requestOptions != null) {
                load.apply(requestOptions);
            }
            load.into(target);
        } else if (target != null) {
            target.onLoadFailed((Drawable) null);
        }
    }

    public final void downToCache(@NotNull Context context, @Nullable String str, @Nullable Integer num, @Nullable Integer num2) {
        if (!isDestroy(context) && !TextUtils.isEmpty(str)) {
            if (num == null || num2 == null) {
                Glide.with(context).load(str).submit();
            } else {
                Glide.with(context).load(str).submit(num.intValue(), num2.intValue());
            }
        }
    }

    @WorkerThread
    @Nullable
    public static Bitmap getFromCache$default(GlideHelper glideHelper, Context context, String str, RequestOptions requestOptions, Long l, int i, Object obj) {
        if ((i & 4) != 0) {
            requestOptions = null;
        }
        return glideHelper.getFromCache(context, str, requestOptions, l);
    }

    @WorkerThread
    @Nullable
    public final Bitmap getFromCache(@NotNull Context context, @Nullable String str, @Nullable RequestOptions requestOptions, @Nullable Long l) {
        if (isDestroy(context) || TextUtils.isEmpty(str)) {
            return null;
        }
        RequestBuilder load = Glide.with(context).asBitmap().load(str);
        Intrinsics.checkExpressionValueIsNotNull(load, "Glide.with(context)\n    …               .load(url)");
        if (requestOptions != null) {
            load.apply(requestOptions);
        }
        FutureTarget submit = load.submit();
        Intrinsics.checkExpressionValueIsNotNull(submit, "requestBuilder.submit()");
        if (l != null) {
            try {
                return (Bitmap) submit.get(l.longValue(), TimeUnit.MILLISECONDS);
            } catch (ExecutionException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (TimeoutException e) {
                e.printStackTrace();
            }
        }
        try {
            return (Bitmap) submit.get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }

    private final boolean isDestroy(Context context) {
        if (context instanceof FragmentActivity) {
            FragmentActivity fragmentActivity = (FragmentActivity) context;
            if (fragmentActivity.isFinishing() || fragmentActivity.isDestroyed()) {
                return true;
            }
            return false;
        } else if (!(context instanceof Activity)) {
            return false;
        } else {
            Activity activity = (Activity) context;
            if (activity.isFinishing() || activity.isDestroyed()) {
                return true;
            }
            return false;
        }
    }

    public final void clear(@NotNull Context context, @NotNull View view) {
        Glide.with(context).clear(view);
    }

    public final void clear(@NotNull View view) {
        Glide.with(view.getContext()).clear(view);
    }

    public final void clear(@NotNull Fragment fragment, @NotNull View view) {
        Glide.with(fragment).clear(view);
    }
}
