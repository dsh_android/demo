package com.example.banmademo.utils;

/**
 * Create by kty
 * on 2020/5/9
 */
import android.graphics.Color;
import android.text.TextUtils;

import androidx.annotation.ColorInt;


public class ColorUtil {

    public static int excessiveColor(String str, String str2, float f) {
        try {
            if (str.startsWith(Constant.POUND_SIGN)) {
                str = str.replace(Constant.POUND_SIGN, "");
            }
            if (str2.startsWith(Constant.POUND_SIGN)) {
                str2 = str2.replace(Constant.POUND_SIGN, "");
            }
            if (str.length() == 6) {
                str = "ff" + str;
            }
            if (str2.length() == 6) {
                str2 = "ff" + str2;
            }
            int parseInt = Integer.parseInt(str.substring(0, 2), 16);
            int parseInt2 = Integer.parseInt(str.substring(2, 4), 16);
            int parseInt3 = Integer.parseInt(str.substring(4, 6), 16);
            int parseInt4 = Integer.parseInt(str.substring(6), 16);
            return Color.argb((int) ((((float) (Integer.parseInt(str2.substring(0, 2), 16) - parseInt)) * f) + ((float) parseInt)), (int) ((((float) (Integer.parseInt(str2.substring(2, 4), 16) - parseInt2)) * f) + ((float) parseInt2)), (int) ((((float) (Integer.parseInt(str2.substring(4, 6), 16) - parseInt3)) * f) + ((float) parseInt3)), (int) ((((float) (Integer.parseInt(str2.substring(6), 16) - parseInt4)) * f) + ((float) parseInt4)));
        } catch (Exception unused) {
            return 0;
        }
    }


    public static int parseColor(String str) {
        if (TextUtils.isEmpty(str)) {
            return 0;
        }
        try {
            return Color.parseColor(str);
        } catch (Exception unused) {
            return 0;
        }
    }

    public static int parseColor(String str, @ColorInt int i) {
        if (TextUtils.isEmpty(str)) {
            return i;
        }
        try {
            return Color.parseColor(str);
        } catch (Exception unused) {
            return i;
        }
    }
}
