package com.example.banmademo.utils;

/**
 * Create by kty
 * on 2020/5/8
 */

public class StringUtils {

    public static boolean isEmpty(String str) {
        int length;
        if (str == null || (length = str.length()) == 0) {
            return true;
        }
        for (int i = 0; i < length; i++) {
            if (!Character.isWhitespace(str.charAt(i))) {
                return false;
            }
        }
        return true;
    }

}
