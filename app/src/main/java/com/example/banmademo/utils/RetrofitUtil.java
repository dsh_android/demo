package com.example.banmademo.utils;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Create by kty
 * on 2020/5/7
 */
public class RetrofitUtil {

    /**
     * 创建请求数据Retrofit
     *
     * @return
     */
    public static Retrofit createRetrofit() {
        return new Retrofit.Builder().baseUrl(Constant.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
    }


    /**
     * 创建上传文件Retrofit
     *
     * @return
     */
    public static Retrofit createUploadRetrofit() {
        return new Retrofit.Builder().baseUrl(Constant.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
    }

}

