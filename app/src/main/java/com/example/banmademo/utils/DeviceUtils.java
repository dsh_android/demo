package com.example.banmademo.utils;

/**
 * Create by kty
 * on 2020/5/8
 */

import android.content.Context;
import android.os.Build;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.WindowManager;

import java.lang.reflect.Method;

public class DeviceUtils {

    /**
     * 获取屏幕宽度
     *
     * @param context
     * @return
     */
    public static int getScreenWidth(Context context) {
        int i;
        Display defaultDisplay = ((WindowManager) context.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
        if (Build.VERSION.SDK_INT >= 17) {
            DisplayMetrics displayMetrics = new DisplayMetrics();
            defaultDisplay.getRealMetrics(displayMetrics);
            i = displayMetrics.widthPixels;
        } else if (Build.VERSION.SDK_INT >= 14) {
            try {
                i = ((Integer) Display.class.getMethod("getRawWidth", new Class[0]).invoke(defaultDisplay, new Object[0])).intValue();
            } catch (Exception unused) {
                i = defaultDisplay.getWidth();
            }
        } else {
            i = defaultDisplay.getWidth();
        }
        return i;
    }

    public static int getScreenHeight(Context context) {
        int i;
        Display defaultDisplay = ((WindowManager) context.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
        if (Build.VERSION.SDK_INT >= 17) {
            DisplayMetrics displayMetrics = new DisplayMetrics();
            defaultDisplay.getRealMetrics(displayMetrics);
            i = displayMetrics.heightPixels;
        } else if (Build.VERSION.SDK_INT >= 14) {
            try {
                Method method = Display.class.getMethod("getRawHeight", new Class[0]);
                Display.class.getMethod("getRawWidth", new Class[0]);
                i = ((Integer) method.invoke(defaultDisplay, new Object[0])).intValue();
            } catch (Exception unused) {
                i = defaultDisplay.getHeight();
            }
        } else {
            i = defaultDisplay.getHeight();
        }
        return i;
    }

    public static int dip2Px(Context context, float dpValue) {
        float scale = context.getResources().getDisplayMetrics().density;
        return (int) (dpValue * scale + 0.5f);
    }

}