package com.example.banmademo.utils;

/**
 * Create by kty
 * on 2020/5/8
 */

import android.text.TextUtils;
import android.widget.ImageView;

import androidx.annotation.NonNull;

public class ImageConvertUtil {
    public static String convertUrlWithView(@NonNull String str, ImageView imageView) {
        int i;
        int i2;
        if (TextUtils.isEmpty(str)) {
            return "";
        }
        if (str.endsWith(".gif") || str.contains("!")) {
            return str;
        }
        if (imageView != null) {
            if (imageView.getLayoutParams() == null) {
                i = imageView.getMeasuredWidth();
            } else {
                i = imageView.getLayoutParams().width;
            }
            if (imageView.getLayoutParams() == null) {
                i2 = imageView.getMeasuredWidth();
            } else {
                i2 = imageView.getLayoutParams().height;
            }
            if (i > 0) {
                str = str + "?x-oss-process=image/resize,w_" + i;
            } else if (i2 > 0) {
                str = str + "?x-oss-process=image/resize,h_" + i2;
            }
        }
        if (str.contains("?x-oss-process=image/")) {
            return str + "/quality,q_100/sharpen,60/format,webp";
        }
        return str + "?x-oss-process=image/quality,q_100/sharpen,60/format,webp";
    }

    public static String convertUrlWithWidth(@NonNull String str, int i) {
        if (TextUtils.isEmpty(str)) {
            return "";
        }
        if (str.endsWith(".gif") || str.contains("!")) {
            return str;
        }
        if (i > 0) {
            return str + "?x-oss-process=image/resize,w_" + i + "/quality,q_100/sharpen,60/format,webp";
        }
        return str + "?x-oss-process=image/quality,q_100/sharpen,60/format,webp";
    }

    public static String convertUrlWithHeight(@NonNull String str, int i) {
        if (TextUtils.isEmpty(str)) {
            return "";
        }
        if (str.endsWith(".gif") || str.contains("!")) {
            return str;
        }
        if (i > 0) {
            return str + "?x-oss-process=image/resize,h_" + i + "/quality,q_100/sharpen,60/format,webp";
        }
        return str + "?x-oss-process=image/quality,q_100/sharpen,60/format,webp";
    }

    public static String convertUrlWithWebp(@NonNull String str) {
        if (TextUtils.isEmpty(str)) {
            return "";
        }
        if (str.endsWith(".gif") || str.contains("!")) {
            return str;
        }
        if (str.contains("?")) {
            return str + "&x-oss-process=image/quality,q_100/sharpen,60/format,webp";
        }
        return str + "?x-oss-process=image/quality,q_100/sharpen,60/format,webp";
    }

    public static String convertUrlWithWebp(@NonNull String str, int i) {
        if (TextUtils.isEmpty(str)) {
            return "";
        }
        if (str.endsWith(".gif") || str.contains("!")) {
            return str;
        }
        return str + String.format("?x-oss-process=image/quality,q_%d/sharpen,60/format,webp", new Object[]{Integer.valueOf(i)});
    }
}