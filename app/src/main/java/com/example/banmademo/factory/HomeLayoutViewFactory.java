package com.example.banmademo.factory;

import android.content.Context;
import android.view.View;

import androidx.annotation.NonNull;

import com.alibaba.android.vlayout.LayoutViewFactory;
import com.example.banmademo.ui.activity.MainActivity;

/**
 * Create by kty
 * on 2020/5/8
 */
public final class HomeLayoutViewFactory implements LayoutViewFactory {

    public static final HomeLayoutViewFactory INSTANCE = new HomeLayoutViewFactory();

    @Override
    public View generateLayoutView(@NonNull Context context) {
        return MainActivity.initXImageView(context);
    }

}
