package com.example.banmademo.home.view;

import com.example.banmademo.bean.HomeImageMessageEntity;
import com.example.banmademo.bean.MallAtmosphereResponse;
import com.example.banmademo.bean.MallBannerResponse;
import com.example.banmademo.bean.MallCountdownResponse;
import com.example.banmademo.bean.MallCustomResponse;
import com.example.banmademo.bean.MallIconResponse;
import com.example.banmademo.bean.TodayRecommendResponse;

/**
 * Create by kty
 * on 2020/5/7
 */
public interface HomeImageView {

    void getHomeImageMessage(HomeImageMessageEntity homeImageMessageEntity);


    void showFirstData(MallAtmosphereResponse mallAtmosphereResponse, MallBannerResponse mallBannerResponse, MallIconResponse mallIconResponse, MallCountdownResponse mallCountdownResponse, MallCountdownResponse mallCountdownResponse2, TodayRecommendResponse todayRecommendResponse, MallCustomResponse mallCustomResponse);

}
