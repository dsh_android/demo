package com.example.banmademo.home.presenter;

import com.example.banmademo.base.OnLoadListener;
import com.example.banmademo.bean.HomeImageMessageEntity;
import com.example.banmademo.bean.MallAtmosphereResponse;
import com.example.banmademo.bean.MallBannerResponse;
import com.example.banmademo.bean.MallCountdownResponse;
import com.example.banmademo.bean.MallCustomResponse;
import com.example.banmademo.bean.MallIconResponse;
import com.example.banmademo.bean.ResultBaseEntity;
import com.example.banmademo.bean.TodayRecommendResponse;
import com.example.banmademo.home.model.HomeImageModel;
import com.example.banmademo.home.model.HomeImageModelImp;
import com.example.banmademo.home.view.HomeImageView;
import com.example.banmademo.presenter.IBasePresenter;

/**
 * Create by kty
 * on 2020/5/7
 */
public class HomeImagePresenter<T> extends IBasePresenter<HomeImageView> {

    private HomeImageView homeImageView;
    private HomeImageModel homeImageModel = new HomeImageModelImp();


    public HomeImagePresenter(HomeImageView homeImageView) {
        this.homeImageView = homeImageView;
    }

    public void getHomeImageMessage() {
        if (homeImageModel != null) {
            homeImageModel.getHomeImageMessage(new OnLoadListener() {
                @Override
                public void onComplete(ResultBaseEntity entity) {
                    homeImageView.getHomeImageMessage((HomeImageMessageEntity) entity);
                }

                @Override
                public void showFirstData(MallAtmosphereResponse mallAtmosphereResponse, MallBannerResponse mallBannerResponse, MallIconResponse mallIconResponse, MallCountdownResponse mallCountdownResponse, MallCountdownResponse mallCountdownResponse2, TodayRecommendResponse todayRecommendResponse, MallCustomResponse mallCustomResponse) {

                }
            });
        }
    }


    public void showFirstData() {
        if (homeImageModel != null) {
            homeImageModel.showFirstData(new OnLoadListener() {
                @Override
                public void onComplete(ResultBaseEntity entity) {

                }

                @Override
                public void showFirstData(MallAtmosphereResponse mallAtmosphereResponse, MallBannerResponse mallBannerResponse, MallIconResponse mallIconResponse, MallCountdownResponse mallCountdownResponse, MallCountdownResponse mallCountdownResponse2, TodayRecommendResponse todayRecommendResponse, MallCustomResponse mallCustomResponse) {
                    homeImageView.showFirstData(mallAtmosphereResponse, mallBannerResponse, mallIconResponse, mallCountdownResponse, mallCountdownResponse2, todayRecommendResponse, mallCustomResponse);
                }
            });
        }
    }

}
