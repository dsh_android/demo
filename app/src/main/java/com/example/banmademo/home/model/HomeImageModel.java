package com.example.banmademo.home.model;

import com.example.banmademo.base.OnLoadListener;
import com.example.banmademo.bean.MallAtmosphereResponse;
import com.example.banmademo.bean.MallBannerResponse;
import com.example.banmademo.bean.MallCountdownResponse;
import com.example.banmademo.bean.MallCustomResponse;
import com.example.banmademo.bean.MallIconResponse;
import com.example.banmademo.bean.TodayRecommendResponse;

/**
 * Create by kty
 * on 2020/5/7
 */
public interface HomeImageModel {

    void getHomeImageMessage(OnLoadListener loadListener);

    void showFirstData(OnLoadListener loadListener);

}
