package com.example.banmademo.home.model;

import android.text.TextUtils;
import android.util.Log;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.example.banmademo.base.OnLoadListener;
import com.example.banmademo.bean.HomeImageMessageEntity;
import com.example.banmademo.bean.MallAtmosphereResponse;
import com.example.banmademo.bean.MallBannerResponse;
import com.example.banmademo.bean.MallCountdownResponse;
import com.example.banmademo.bean.MallCustomResponse;
import com.example.banmademo.bean.MallIconResponse;
import com.example.banmademo.bean.ResultBaseEntity;
import com.example.banmademo.bean.TodayRecommendResponse;
import com.example.banmademo.home.HomeApi;
import com.example.banmademo.utils.ListUtils;
import com.example.banmademo.utils.RetrofitUtil;


import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Create by kty
 * on 2020/5/7
 */
public class HomeImageModelImp implements HomeImageModel {

    @Override
    public void getHomeImageMessage(OnLoadListener loadListener) {
        Retrofit retrofit = RetrofitUtil.createRetrofit();
        HomeApi meApi = retrofit.create(HomeApi.class);
        Call<HomeImageMessageEntity> call = meApi.getHomeData();
        call.enqueue(new Callback<HomeImageMessageEntity>() {
            @Override
            public void onResponse(Call<HomeImageMessageEntity> call, Response<HomeImageMessageEntity> response) {

                if (response.isSuccessful()) {
                    if (response.code() == 0) {
                        loadListener.onComplete(response.body());
                    }
                }
            }

            @Override
            public void onFailure(Call<HomeImageMessageEntity> call, Throwable t) {
            }
        });

    }

    @Override
    public void showFirstData(OnLoadListener loadListener) {
        Retrofit retrofit = RetrofitUtil.createRetrofit();
        HomeApi meApi = retrofit.create(HomeApi.class);
        Call<ResultBaseEntity> call = meApi.getFirstData();
        call.enqueue(new Callback<ResultBaseEntity>() {
            @Override
            public void onResponse(Call<ResultBaseEntity> call, Response<ResultBaseEntity> response) {

                Log.e("--kty--", "--1111--");
//                if (response.code() == 0) {
//
//
//                }

                Log.e("--kty--", "--2222--");


                MallCustomResponse mallCustomResponse;
                TodayRecommendResponse todayRecommendResponse;
                MallCountdownResponse mallCountdownResponse;
                MallCountdownResponse mallCountdownResponse2;
                MallIconResponse mallIconResponse;
                MallBannerResponse mallBannerResponse;
                MallAtmosphereResponse mallAtmosphereResponse;
                MallCustomResponse mallCustomResponse2;
                TodayRecommendResponse todayRecommendResponse2;
                MallCountdownResponse mallCountdownResponse3;
                MallCountdownResponse mallCountdownResponse4;
                MallIconResponse mallIconResponse2;
                MallBannerResponse mallBannerResponse2;
//                Object obj2 = response.body().getData().toString();
                Object obj2 = JSON.toJSONString(response.body().getData());

                if ((obj2 instanceof String)) {
                    Log.e("--kty--", "--3333--");
                    MallAtmosphereResponse mallAtmosphereResponse2 = null;
                    try {
                        JSONArray jSONArray = JSONObject.parseObject((String) obj2).getJSONArray("dataList");

                        Log.e("--kty--", "--4444--");
                        if (!ListUtils.isEmpty(jSONArray)) {
                            int i = 0;
                            mallBannerResponse2 = null;
                            mallIconResponse2 = null;
                            mallCountdownResponse4 = null;
                            mallCountdownResponse3 = null;
                            todayRecommendResponse2 = null;
                            mallCustomResponse2 = null;
                            while (i < jSONArray.size()) {
                                try {
                                    String str = (String) jSONArray.getJSONObject(i).get("code");
                                    if (TextUtils.equals(str, "101105923976")) {
                                        mallAtmosphereResponse2 = (MallAtmosphereResponse) jSONArray.getObject(i, MallAtmosphereResponse.class);
                                    } else if (TextUtils.equals(str, "100155957578")) {
                                        mallBannerResponse2 = (MallBannerResponse) jSONArray.getObject(i, MallBannerResponse.class);
                                    } else if (TextUtils.equals(str, "100936418014")) {
                                        mallIconResponse2 = (MallIconResponse) jSONArray.getObject(i, MallIconResponse.class);
                                    } else if (TextUtils.equals(str, "100256892947")) {
                                        mallCountdownResponse4 = (MallCountdownResponse) jSONArray.getObject(i, MallCountdownResponse.class);
                                    } else if (TextUtils.equals(str, "100592783913")) {
                                        mallCountdownResponse3 = (MallCountdownResponse) jSONArray.getObject(i, MallCountdownResponse.class);
                                    } else if (TextUtils.equals(str, "300946076619")) {
                                        todayRecommendResponse2 = (TodayRecommendResponse) jSONArray.getObject(i, TodayRecommendResponse.class);
                                    } else if (TextUtils.equals(str, "100128616589")) {
                                        mallCustomResponse2 = (MallCustomResponse) jSONArray.getObject(i, MallCustomResponse.class);
                                    }
                                    i++;
                                } catch (Exception e) {
                                    e = e;
                                    e.printStackTrace();
                                    mallAtmosphereResponse = mallAtmosphereResponse2;
                                    mallBannerResponse = mallBannerResponse2;
                                    mallIconResponse = mallIconResponse2;
                                    mallCountdownResponse2 = mallCountdownResponse4;
                                    mallCountdownResponse = mallCountdownResponse3;
                                    todayRecommendResponse = todayRecommendResponse2;
                                    mallCustomResponse = mallCustomResponse2;

                                    loadListener.showFirstData(mallAtmosphereResponse, mallBannerResponse, mallIconResponse, mallCountdownResponse2, mallCountdownResponse, todayRecommendResponse, mallCustomResponse);
                                }
                            }
                        } else {
                            mallBannerResponse2 = null;
                            mallIconResponse2 = null;
                            mallCountdownResponse4 = null;
                            mallCountdownResponse3 = null;
                            todayRecommendResponse2 = null;
                            mallCustomResponse2 = null;
                        }
                        mallAtmosphereResponse = mallAtmosphereResponse2;
                        mallBannerResponse = mallBannerResponse2;
                        mallIconResponse = mallIconResponse2;
                        mallCountdownResponse2 = mallCountdownResponse4;
                        mallCountdownResponse = mallCountdownResponse3;
                        todayRecommendResponse = todayRecommendResponse2;
                        mallCustomResponse = mallCustomResponse2;
                    } catch (Exception e2) {
//                                e = e2;
                        mallBannerResponse2 = null;
                        mallIconResponse2 = null;
                        mallCountdownResponse4 = null;
                        mallCountdownResponse3 = null;
                        todayRecommendResponse2 = null;
                        mallCustomResponse2 = null;
                        e2.printStackTrace();
                        mallAtmosphereResponse = mallAtmosphereResponse2;
                        mallBannerResponse = mallBannerResponse2;
                        mallIconResponse = mallIconResponse2;
                        mallCountdownResponse2 = mallCountdownResponse4;
                        mallCountdownResponse = mallCountdownResponse3;
                        todayRecommendResponse = todayRecommendResponse2;
                        mallCustomResponse = mallCustomResponse2;
                        loadListener.showFirstData(mallAtmosphereResponse, mallBannerResponse, mallIconResponse, mallCountdownResponse2, mallCountdownResponse, todayRecommendResponse, mallCustomResponse);
                    }
                    loadListener.showFirstData(mallAtmosphereResponse, mallBannerResponse, mallIconResponse, mallCountdownResponse2, mallCountdownResponse, todayRecommendResponse, mallCustomResponse);
                }
            }

            @Override
            public void onFailure(Call<ResultBaseEntity> call, Throwable t) {
                Log.e("--kty--", "--失败--");
                t.printStackTrace();
                Log.e("--kty--", "--失败--");
            }
        });
    }

}
