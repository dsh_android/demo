package com.example.banmademo.home;

import com.example.banmademo.bean.HomeImageMessageEntity;
import com.example.banmademo.bean.ResultBaseEntity;

import io.reactivex.rxjava3.core.Observable;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

/**
 * Create by kty
 * on 2020/5/7
 */
public interface HomeApi {

//    @FormUrlEncoded
//    @POST("ruban/api/res/hp")
//    Observable<HomeImageMessageEntity> getHomeImage(@Field("token") String token);


    @GET("ruban/api/res/hp")
    Call<HomeImageMessageEntity> getHomeData();

    @GET("ruban/api/res/hp")
    Call<ResultBaseEntity> getFirstData();
}
